<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Retender extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('string', 'text'));
        $this->load->model('approval/M_approval');
        $this->load->model('vendor/M_send_invitation')->model('vendor/M_vendor');
        $this->load->model('setting/M_master_department');
        $this->load->model('procurement/M_msr', 'msr');
        $this->load->model('approval/M_bl');
        $this->load->model('approval/T_upload');
        $this->load->model('procurement/M_msr_item', 'msr_item');
        $this->load->model('procurement/M_msr_attachment', 'msr_attachment');
        $this->load->model('user/M_view_user');
        $this->load->helper('exchange_rate_helper')->helper(array('form', 'array', 'url', 'exchange_rate'));
        $this->load->model('vn/info/M_vn', 'mvn');
        $this->load->model('m_base');
        $this->load->model('procurement/M_retender');
        $get_menu = $this->M_vendor->menu();
        $this->menu = array();
        foreach ($get_menu as $k => $v) {
            $this->menu[$v->PARENT][$v->ID_MENU]['ICON'] = $v->ICON;
            $this->menu[$v->PARENT][$v->ID_MENU]['URL'] = $v->URL;
            $this->menu[$v->PARENT][$v->ID_MENU]['DESKRIPSI_IND'] = $v->DESKRIPSI_IND;
            $this->menu[$v->PARENT][$v->ID_MENU]['DESKRIPSI_ENG'] = $v->DESKRIPSI_ENG;
        }
    }

    public function index($value = '')
    {
        $data['menu'] = $this->menu;
        $this->load->library('pagination');
        $data['eds'] = $this->getEdList();
        $config['base_url'] = base_url('approval/retender/index'); //site url
        $config['total_rows'] = $data['eds']->num_rows(); //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 4;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data['data'] = $this->getEdListLimit($config["per_page"], $data['page']);

//        echo $data['data'];
//        die();

        $data['pagination'] = $this->pagination->create_links();

        $this->template->display('retender/index', $data);
    }

    public function getEdList()
    {
        $eds = $this->db->select("trash_t_eq_data.*,replace(trash_t_eq_data.msr_no,'OR','OQ') ed_no")->where(['status' => 1])->order_by('msr_no', 'desc')->get('trash_t_eq_data');
//      $lastquery = $this->db->last_query();
        return $eds;
    }

    public function getEdListLimit($limit, $start)
    {
        $user = user();
        $roles = explode(",", $user->ROLES);
        $roles = array_values(array_filter($roles));

        if ($user->ID_USER == 164 or $user->ID_USER == 165 or $user->ID_USER == 166 or $user->ID_USER == 167 or in_array(bled, $roles) or in_array(proc_committe, $roles)) {
            if ($this->input->get('user')) {
                $this->db->where('t_assignment.user_id = ' . $this->input->get('user'));
                // echo "<pre>";
                $getAssignmentAgreement = $this->getAssignmentAgreement($this->input->get('user'));
                // echo count($getAssignmentAgreement);
                // echo "<br>";
                /*exit();*/
                if (is_array($getAssignmentAgreement)) {
                    $this->db->where_in('t_msr.msr_no', $getAssignmentAgreement);
                }
                $this->db->where_in('t_msr.status', [0, 1]);
            }
        } else {
            $eds = $this->db->where('t_msr.ID_DEPARTMENT = ' . $user->ID_DEPARTMENT);
        }
        $eds = $this->db->select("trash_t_eq_data.*, t_msr.id_currency, t_msr.status as msr_stts, t_msr.total_amount, m_currency.CURRENCY,replace(trash_t_eq_data.msr_no,'OR','OQ') ed_no, m_departement.DEPARTMENT_DESC as department, specialist.NAME as specialist, (CASE WHEN approval.approval_posisition IS NULL THEN 'Completed' ELSE approval.approval_posisition END) as approval_posisition")
            ->join('t_msr', 't_msr.msr_no = trash_t_eq_data.msr_no', 'left')
            ->join('m_user', 'm_user.ID_USER = t_msr.create_by', 'left')
            ->join('m_departement', 'm_departement.ID_DEPARTMENT = t_msr.ID_DEPARTMENT', 'left')
            ->join('t_assignment', 't_assignment.msr_no = t_msr.msr_no', 'left')
            ->join('m_user as specialist', 'specialist.ID_USER = t_assignment.user_id', 'left')
            ->join('(
            SELECT t_approval.*, m_user_roles.DESCRIPTION as approval_posisition FROM
            (
                SELECT
                    data_id,
                    MIN(t_approval.urutan) as urutan
                FROM t_approval
                JOIN m_approval ON m_approval.id = t_approval.m_approval_id
                WHERE m_approval.module_kode = \'msr_spa\'
                AND (t_approval.status = 0 OR t_approval.status = 2)
                GROUP BY data_id
            ) as approval
            JOIN (
                SELECT
                    t_approval.*, m_approval.module_kode, m_approval.role_id
                FROM t_approval
                JOIN m_approval ON m_approval.id = t_approval.m_approval_id
                WHERE m_approval.module_kode = \'msr_spa\'
            ) t_approval ON t_approval.data_id = approval.data_id AND t_approval.urutan = approval.urutan
            JOIN m_user_roles ON m_user_roles.ID_USER_ROLES = t_approval.role_id
        ) approval', 'approval.data_id = trash_t_eq_data.msr_no', 'left')
            ->join('m_currency', 'm_currency.ID = t_msr.id_currency')
            // ->where_in('t_msr.status', [0,1])
            ->order_by('msr_no', 'desc')->get('(select * from trash_t_eq_data where status = 1) trash_t_eq_data');
//        $query = $this->db->last_query();
        if ($this->input->get('debug')) {
            echo "<pre>";
            echo "num : " . $eds->num_rows();
            echo "<br> : " . $this->db->last_query();
            exit();
        }
        return $eds;
    }

    public function getAssignmentAgreement($id_user = '')
    {
        $this->db->select('t_assignment.*');
        $this->db->where(['t_assignment.user_id' => $id_user]);
        $t_assignment = $this->db->get('t_assignment');
        $msr_no = [];
        foreach ($t_assignment->result() as $key => $value) {
            $msr_no[] = $value->msr_no;
        }
        $t_purchase_order = 0;

        if (count($msr_no) > 0) {
            $t_purchase_order = $this->db->where_in('msr_no', $msr_no)->where(['issued' => 1])->get('t_purchase_order');
            $msrArray = [];
            foreach ($t_purchase_order->result() as $key => $value) {
                $msrArray[] = $value->msr_no;
            }
            $newMsr = [];
            foreach ($msr_no as $key => $value) {
                if (!in_array($value, $msrArray)) {
                    $newMsr[] = $value;
                }
            }
            return $newMsr;
        } else {
            return false;
        }
    }
    public function show($msr_no = '', $approval_id = '')
    {
        $get_menu = $this->M_vendor->menu();
        $dt = array();
        foreach ($get_menu as $k => $v) {
            $dt[$v->PARENT][$v->ID_MENU]['ICON'] = $v->ICON;
            $dt[$v->PARENT][$v->ID_MENU]['URL'] = $v->URL;
            $dt[$v->PARENT][$v->ID_MENU]['DESKRIPSI_IND'] = $v->DESKRIPSI_IND;
            $dt[$v->PARENT][$v->ID_MENU]['DESKRIPSI_ENG'] = $v->DESKRIPSI_ENG;
        }
        $data['menu'] = $dt;

        $greetings = $this->M_approval->greetings_msr();
        // $data['greetings'] = $greetings;
        $data['idnya'] = $approval_id;
        $data['msr_no'] = $msr_no;
        $data['msr'] = $this->db->select('t_msr.*,m_company.DESCRIPTION company_name,m_msrtype.MSR_DESC msr_name, m_plocation.PGroup_Desc proc_location_name, t_msr.costcenter_desc cost_center_name, m_pmethod.PMETHOD_DESC proc_method_name, m_currency.CURRENCY currency')
            ->join('m_company', 'm_company.ID_COMPANY=t_msr.id_company')
            ->join('m_msrtype', 'm_msrtype.ID_MSR=t_msr.id_msr_type')
            ->join('m_plocation', 'm_plocation.ID_Pgroup=t_msr.id_ploc')
            ->join('m_user', 'm_user.ID_USER=t_msr.create_by')
            ->join('m_costcenter', 'm_costcenter.ID_COSTCENTER=m_user.COST_CENTER')
            ->join('m_pmethod', 'm_pmethod.ID_PMETHOD=t_msr.id_pmethod')
            ->join('m_currency', 'm_currency.ID=t_msr.id_currency')
            ->where('msr_no', $msr_no)->get('t_msr')->row();
        $data['approval'] = $this->db->select('trash_t_approval.*,m_approval.role_id')
            ->join('m_approval', 'm_approval.id=trash_t_approval.m_approval_id')
            ->where('trash_t_approval.id', $approval_id)
            ->get('trash_t_approval')->row();
        // echo $this->db->last_query();
        $data['method'] = $this->db->get('m_pmethod')->result();
        $data['doc'] = $this->db->where(['module_kode' => 'bled', 'data_id' => $msr_no])->get('trash_t_upload')->result();
        $data['t_bl'] = $this->db->where(['msr_no' => $msr_no])->get('trash_t_bl')->row();
        $data['ed'] = $this->M_retender->getTrashEdFromMsr($msr_no)->row();

        $this->session->set_userdata('referred_from', current_url());
        // $data['edapproved']  = $this->M_approval->edapproved($approval_id);
        $data['js'] = $this->load->view('approval/devbledjs', array(), true);
        $data['readyIssude'] = $this->M_approval->readyIssude($msr_no);
        $data['viewaja'] = $approval_id > 0 ? true : false;
        $data['tbmi2'] = $this->M_retender->tbmi2Trash($msr_no);
        /*print_r($data['ed']);
        exit();*/
        $this->template->display('retender/show', $data);
    }

    public function dtBlSession($msr_no = '', $delete = 1)
    {
      $no = 1;
      $tb = '';

      foreach ($this->db->where('msr_no', $msr_no)->get('trash_t_bl_detail')->result() as $key => $value) {
          $vendor = $this->db->where('ID', $value->vendor_id)->get('m_vendor')->row();
          $address = $this->db->where(['BRANCH_TYPE' => 'KANTOR PUSAT', 'ID_VENDOR' => $value->vendor_id])->get('m_vendor_address')->row();
          $alamatKantor = @$address->ADDRESS;
          $contact = $this->db->where(['KEYS' => 1, 'ID_VENDOR' => $value->vendor_id])->get('m_vendor_contact')->row();
          $contactName = @$contact->NAMA;
          $contactMobile = @$contact->TELP;
          $contactEmail = @$contact->EMAIL;
          if ($vendor) {
              $tb .= "<tr><td>" . $no++ . "</td>
              <td>$vendor->NAMA</td>
              <td>$vendor->NO_SLKA</td>
              <td>$alamatKantor</td>
              <td>$contactName</td><td>$contactMobile</td><td>$contactEmail</td>";
              if ($delete) {
                  $tb .= "<td class='text-center'><a href='javascript:void()' onclick='hapusBlClick($value->id)' class='btn btn-sm btn-danger'>Delete</a></td>";
              }
              $tb .= "</tr>";
          }
      }
      echo $tb;
  }

  public function sop_grid($value = '')
  {
      $msr_no = $this->input->post('msr_no');
      $type = $this->input->post('type');
      $sop_grid = $this->M_retender->sop_get(['trash_t_sop.msr_no' => $msr_no]);
      $ed = $this->M_retender->getEd($msr_no);
      if ($ed->num_rows() > 0) {
          $ed = $ed->row();
          $currency = $this->M_bl->getMc($ed->currency)->row();
          $cur = $currency->CURRENCY;
      } else {
          $cur = '';
      }
      $bgGrey = "style='background:none'";
      if ($sop_grid->num_rows() > 0) {
          $sop_grid = $sop_grid->result();
          if ($sop_grid[0]->sop_type == 2) {
              $qty1 = 'Qty 1';
              $uom1 = 'UoM 1';
          } else {
              $qty1 = 'Qty';
              $uom1 = 'UoM';
          }
          $tb = "<thead><tr $bgGrey><th>No</th><th>MSR Item</th><th>Item</th><th class='text-center'>$qty1</th><th class='text-center'>$uom1</th>";
          if ($sop_grid[0]->sop_type == 2) {
              $tb .= "<th class='text-center'>Qty 2</th><th class='text-center'>UoM 2</th>";
          }
          /*for view mode*/
          if ($type == 'view') {
              $tb .= "<th class='text-center'>Currency</th><th class='text-right'>Unit Price</th><th class='text-right'>Total Value</th><th>Cost Center</th><th>Account Subsidiary</th><th>Inv Type</th><th>Item Modification</th></tr></thead>";
          } else {
              $tb .= "<th class='text-center'>Currency</th><th class='text-right'>Unit Price</th><th class='text-right'>Total Value</th><th>Cost Center</th><th>Account Subsidiary</th><th>Inv Type</th><th class='text-center'>Item Modification</th><th class='text-center'>Action</th></tr></thead>";
          }
          $tb .= "<tbody>";
          $no = 1;
          foreach ($sop_grid as $r) {
              $uom1 = $r->uom1;
              $m_material_uom = $this->db->where(['MATERIAL_UOM' => $uom1])->get('m_material_uom')->row();
              $uom1 .= " - $m_material_uom->DESCRIPTION";
              $tb .= "<tr><td $bgGrey>$no</td><td>$r->description</td><td>$r->item</td><td class='text-center'>$r->qty1</td><td class='text-center'>$uom1</td>";
              if ($r->sop_type == 2) {
                  $m_material_uom = $this->db->where(['MATERIAL_UOM' => $r->uom2])->get('m_material_uom')->row();
                  $qty2 = $r->qty2 > 0 ? $r->qty2 : '-';
                  $uom2 = $r->qty2 > 0 ? $r->uom2 . " - $m_material_uom->DESCRIPTION" : '-';
                  $tb .= "<td class='text-center'>$qty2</td><td class='text-center'>$uom2</td>";
              }
              $tax = $r->tax == 1 ? "Yes" : "No";
              $edit = "<a href='#' class='btn btn-sm btn-warning' onclick='editSopClick($r->id)'>Edit</a>";
              $delete = "<a href='#' class='btn btn-sm btn-danger' onclick='deleteSopClick($r->id)'>Delete</a>";
              /*for view mode*/
              if ($type == 'view') {
                  $is_modif = $r->item_modification == 1 ? 'Yes' : 'No';
                  $tb .= "<td $bgGrey align='center'></td><td $bgGrey></td><td $bgGrey></td>
                  <td>$r->costcenter_desc</td>
                  <td>$r->accsub_desc</td>
                  <td>$r->inv_desc</td>
                  <td class='text-center'>$is_modif</td>
                  </tr>";
              } else {
                  $is_modif = $r->item_modification == 1 ? 'Yes' : 'No';
                  $tb .= "<td $bgGrey align='center'></td><td $bgGrey></td><td $bgGrey></td>
                  <td>$r->costcenter_desc</td>
                  <td>$r->accsub_desc</td>
                  <td>$r->inv_desc</td>
                  <td class='text-center'>$is_modif</td>
                  <td class='text-center'>$edit $delete</td>
                  </tr>";
              }
              $no++;
          }
          $tb .= "</tbody>";
          echo $tb;
      } else {
          echo "<center>No Data</center>";
      }
    }
}