<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msr_draft extends CI_Controller {

	public function __construct() {
        parent::__construct();
    }

    public function delete($id='')
    {
    	$this->load->model('procurement/M_msr_draft');

    	$this->db->trans_begin();

    	$this->M_msr_draft->deletedrafbyid($id);
    	$this->M_msr_draft->delAttr($id);

    	if($this->db->trans_status() === true)
    	{
    		$this->db->trans_commit();
	        $this->session->set_tempdata('message', array(
	            'message' => 'MSR Draft Deleted',
	            'type' => 'success'
	        ), 30);
    		echo json_encode(['status'=>true]);
    	}
    	else
    	{
    		$this->db->trans_rollback();
    		echo json_encode(['msg'=>'Failed, Please try again or Call Administratior']);
    	}
    }
}