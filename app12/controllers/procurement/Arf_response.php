<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Arf_response extends CI_Controller {

    protected $document_path = 'upload/ARF';
    protected $document_allowed_types = 'jpg|jpeg|pdf|doc|docx';
    protected $document_max_size = '2048';

    public function __construct() {
        parent::__construct();

        $this->load->model('vn/info/M_all_vendor');

        $this->load->library('url_generator');
        $this->load->library('redirect');
        $this->load->library('form');
        $this->load->model('m_base');
        $this->load->model('procurement/m_procurement');
        $this->load->model('procurement/arf/m_arf');
        $this->load->model('procurement/arf/m_arf_assignment');
        $this->load->model('procurement/arf/m_arf_attachment');
        $this->load->model('procurement/arf/m_arf_detail');
        $this->load->model('procurement/arf/m_arf_detail_reason');
        $this->load->model('procurement/arf/m_arf_detail_revision');
        $this->load->model('procurement/arf/m_arf_po');
        $this->load->model('procurement/arf/m_arf_po_detail');
        $this->load->model('procurement/arf/m_arf_notification_trash');
        $this->load->model('procurement/arf/m_arf_notification_detail_revision_trash');
        $this->load->model('procurement/arf/m_arf_notification_attachment_trash');
        $this->load->model('procurement/arf/m_arf_sop_trash');
        $this->load->model('procurement/arf/m_arf_response');
        $this->load->model('procurement/arf/m_arf_response_detail');
        $this->load->model('procurement/arf/m_arf_response_attachment_trash');

        $this->load->helper('exchange_rate_helper');
    }

    public function view_trash($id) {
        $arf = $this->m_arf_notification_trash->view('arf_responsed_trash')->scope(array('responsed_trash'))->find($id);
        // print_r($arf)
        // exit();
        
        $arf->item = $this->m_arf_sop_trash->view('response_trash')->where('trash_t_arf_sop.doc_id', $id)->get();
        // exit();
        foreach ($this->m_arf_notification_detail_revision_trash->where('doc_id', $id)->get() as $revision) {
            $arf->revision[$revision->type] = $revision;
        }
        $arf->attachment = $this->m_arf_notification_attachment_trash->view('notification_attachment')->where('trash_t_arf_notification_upload.doc_id', $arf->id)->get();
        $arf->response_attachment = $this->m_arf_response_attachment_trash->where('trash_t_arf_response_attachment.doc_id', $arf->doc_id)->get();
        $po = $this->m_arf_po->where('t_purchase_order.po_no', $arf->po_no)->first();
        $po->po_type = $this->m_arf_po->enum('type', $po->po_type);
        $po->item = $this->m_arf_po_detail->view('po_detail')->where('t_purchase_order_detail.po_id', $po->id)->get();
        $data['arf'] = $arf;
        $data['po'] = $po;
        $data['document_path'] = $this->document_path;
        $this->template->display_vendor('vn/info/arf_notification_view', $data);
    }
}