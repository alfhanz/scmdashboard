<?php
class Msr extends CI_Controller
{
    public function __construct() {
        parent::__construct();        
        $this->load->helper('url');
        $this->load->library('session');        
        $this->load->model('dashboard/M_msr', 'msr');                
        $this->load->model('vendor/M_all_intern', 'mai');                
    }

    public function index() {
        $dt=array();        
        $cek = $this->mai->cek_session(); 
        $compndept=$this->msr->get_compndept();                
        $type=$this->msr->get_data("ID_MSR,MSR_DESC","m_msrtype","status=1",0);   
             
        $method=$this->msr->get_data("ID_PMETHOD,PMETHOD_DESC","m_pmethod","status=1",0);
        $category=$this->msr->get_data("MATERIAL_GROUP,DESCRIPTION","m_material_group","CATEGORY='GROUP'",0);
        $specialist=$this->msr->get_data('ID_USER,NAME','m_user','status=1',1);
        $dt['compndept']=$compndept;
        $dt['type']=$type;
        $dt['method']=$method;
        $dt['category']=$category;
        $dt['spec']=$specialist;        

        $this->template->display_dash('dashboard/V_msr',$dt);
    }    
/* ===========================================-------- API START------- ====================================== */
    public function output($return = array()) {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        exit(json_encode($return));
    }

    public function process()
    {        
        $pos=stripslashes($this->input->post('position'));        
        $dt=array();        
        if(stripslashes($this->input->post('status'))=='%' && $pos!='msr_status'&& $pos!=null)
        {
            
            if($pos=='msr_type')
                $this->msr_type();
            else if($pos=='msr_method')
                $this->msr_method();
            else if($pos=='process')
                $this->show_list();
            else if($pos == 'msr_specialist')
                $this->msr_specialist();
        }
        else{
            
            $tamp_c=stripslashes($this->input->post('company'));
            $tamp=stripslashes($this->input->post('department'));            
            $data=array(
                "company"=>$tamp_c,
                "department"=>$tamp,
                "status"=>stripslashes($this->input->post('status')),
                "type"=>stripslashes($this->input->post('type')),
                "method"=>stripslashes($this->input->post('method')),
                "category"=>stripslashes($this->input->post('category'))
            );
            $y=$this->input->post('year');
            $y2=$this->input->post('year2');
            $month=[date('m'),date('m')];
            if(isset($_POST['month']))
            {
                $month=explode(',',$_POST['month']);
            }            
            $res=$this->msr->get_all($month[0],$month[count($month)-1],$y,$y2,$data);    
            
            $cnt=0;
            $prep=$sel=$sgn=$comp=$cnl='';
            $t_p=$t_s=$t_c=$t_sg=$t_cl='';
            foreach($res as $k => $v)
            {            
                $prep=$sel=$sgn=$comp=$cnl='';
                $t_p=$t_s=$t_c=$t_sg=$t_cl='';
                if($data['status']==1 || $data['status']=='%'){
                    $prep=$v['preparation'];
                    $t_p=$v['t_p'];
                }                
                if($data['status']==2 || $data['status']=='%'){
                    $sel=$v['selection'];                
                    $t_s=$v['t_s'];
                }
                if($data['status']==3 || $data['status']=='%'){
                    $sgn=$v['completed'];                
                    $t_c=$v['t_c'];
                }
                if($data['status']==3 || $data['status']=='%'){
                    $comp=$v['signed'];                
                    $t_sg=$v['t_sg'];
                }
                if($data['status']==4 || $data['status']=='%'){
                    $cnl=$v['canceled'];
                    $t_cl=$v['t_cl'];
                }
                
                
                $dt[$cnt]['period']=$v['period'];            
                $dt[$cnt]['name']=array($prep,$sel,$sgn,$comp,$cnl);
                $dt[$cnt]['value']=array($t_p,$t_s,$t_c,$t_sg,$t_cl);
                $cnt++;
            }
        }
        $this->output($dt);
    }

    public function msr_status()
    {        
        $tamp_c=stripslashes($this->input->post('company'));
        $tamp=stripslashes($this->input->post('department'));            
        $data=array(
            "company"=>$tamp_c,
            "department"=>$tamp,
            "status"=>stripslashes($this->input->post('status')),
            "type"=>stripslashes($this->input->post('type')),
            "method"=>stripslashes($this->input->post('method')),
            "category"=>stripslashes($this->input->post('category'))
        );
        $y=$this->input->post('year');
        $y2=$this->input->post('year2');
        $month=[date('m'),date('m')];
        if(isset($_POST['month']))
        {
            $month=explode(',',$_POST['month']);
        }            
        $res=$this->msr->get_all($month[0],$month[count($month)-1],$y,$y2,$data);
        $dt=array();
        $cnt=0;
        $cnt2=0;
        foreach($res as $k => $v)
        {            
            $dt[$cnt]['period']=$v['period'];            
            $dt[$cnt]['name']=array($v['preparation'],$v['selection'],$v['completed'],$v['signed'],$v['canceled']);
            $dt[$cnt]['value']=array(
                $v['t_p'] / 1000000000,
                $v['t_s'] / 1000000000,
                $v['t_c'] / 1000000000,
                $v['t_sg'] / 1000000000,
                $v['t_cl'] / 1000000000
            );
            $cnt++;
        }
        return $this->output($dt);
    }

    public function msr_type()
    {      
        
        $tamp_c=stripslashes($this->input->post('company'));
        $tamp=stripslashes($this->input->post('department'));        
        $data=array(
            "company"=>$tamp_c,
            "department"=>$tamp,
            "status"=>stripslashes($this->input->post('status')),
            "type"=>stripslashes($this->input->post('type')),
            "method"=>stripslashes($this->input->post('method')),
            "category"=>stripslashes($this->input->post('category'))
        );
        $y=$this->input->post('year');
        $y2=$this->input->post('year2');
        $month=[date('m'),date('m')];
        if(isset($_POST['month']))
        {
            $month=explode(',',$_POST['month']);
        }            
        $res=$this->msr->get_type($month[0],$month[count($month)-1],$y,$y2,$data);
        $dt=array();
        $cnt=0;
        $cnt2=0;
        foreach($res as $k => $v)
        {            
            $dt[$cnt]['period']=$v['period'];      
            if($data['type']=='GOODS')     
            {
                $dt[$cnt]['name']=array($v['goods'],'','');
                $dt[$cnt]['value']=array($v['t_g'],'','');
            }                 
            else if($data['type']=='SERVICE')
            {
                $dt[$cnt]['name']=array('',$v['services'],'');
                $dt[$cnt]['value']=array('',$v['t_s'],'');
            }
            else if($data['type']=='BLANKET')
            {
                $dt[$cnt]['name']=array('','',$v['blanket']);
                $dt[$cnt]['value']=array('','',$v['t_b']);
            }
            else
            {
                $dt[$cnt]['name']=array($v['goods'],$v['services'],$v['blanket']);
                $dt[$cnt]['value']=array($v['t_g'],$v['t_s'],$v['t_b']);
            }
            $cnt++;
        }
        return $this->output($dt);
    }

    public function msr_method()
    {       
        $tamp_c=stripslashes($this->input->post('company'));
        $tamp=stripslashes($this->input->post('department'));        
        $data=array(
            "company"=>$tamp_c,
            "department"=>$tamp,
            "status"=>stripslashes($this->input->post('status')),
            "type"=>stripslashes($this->input->post('type')),
            "method"=>stripslashes($this->input->post('method')),
            "category"=>stripslashes($this->input->post('category'))
        ); 
        $y=$this->input->post('year');
        $y2=$this->input->post('year2');
        $month=[date('m'),date('m')];
        if(isset($_POST['month']))
        {
            $month=explode(',',$_POST['month']);
        }            
        $res=$this->msr->get_method($month[0],$month[count($month)-1],$y,$y2,$data);
        $dt=array();
        $cnt=0;
        $cnt2=0;
        foreach($res as $k => $v)
        {            
            $dt[$cnt]['period']=$v['period'];            
            $dt[$cnt]['name']=array($v['DA'],$v['DS'],$v['TN']);
            $dt[$cnt]['value']=array($v['t_a'],$v['t_s'],$v['t_n']);
            $cnt++;
        }
        return $this->output($dt);
    }

    public function msr_specialist()
    {
        $tamp_c=stripslashes($this->input->post('company'));
        $tamp=stripslashes($this->input->post('department'));        
        $data=array(
            "company"=>$tamp_c,
            "department"=>$tamp,
            "status"=>stripslashes($this->input->post('status')),
            "type"=>stripslashes($this->input->post('type')),
            "method"=>stripslashes($this->input->post('method')),
            "category"=>stripslashes($this->input->post('category'))
        ); 
        $y=$this->input->post('year');
        $y2=$this->input->post('year2');
        $month=[date('m'),date('m')];
        if(isset($_POST['month']))
        {
            $month=explode(',',$_POST['month']);
        }            
        $res=$this->msr->get_specialist($month[0],$month[count($month)-1],$y,$y2,$data);                
        return $this->output($res);
    }
/* ===========================================-------- get data START------- ====================================== */    

    public function show_list()
    {
        $data=array();
        $y=date('Y');
        $y2=date('Y');
        if($_POST)
        {
            $tamp_c=stripslashes($this->input->post('company'));
            $tamp=stripslashes($this->input->post('department'));        
            $data=array(
                "company"=>$tamp_c,
                "department"=>$tamp,
                "status"=>stripslashes($this->input->post('status')),
                "type"=>stripslashes($this->input->post('type')),
                "method"=>stripslashes($this->input->post('method')),
                "category"=>stripslashes($this->input->post('category'))
            ); 
            $y=$this->input->post('year');
            $y2=$this->input->post('year2');
        }
        else
        {
            $data=array(
                "company"=>'%',
                "department"=>'%',
                "status"=>'%',
                "type"=>'%',
                "method"=>'%',
                "category"=>'%',
            ); 
        }        
        $month=[date('m'),date('m')];
        if(isset($_POST['month']))
        {
            $month=explode(',',$_POST['month']);
        }            
        $res=$this->msr->get_details($month[0],$month[count($month)-1],$y,$y2,$data);
        
        $dt=array();
        if(count($res)>=1)
        {                        
            foreach($res as $k => $v)
            {                               
                $dt[$k][0]=$k+1;
                $dt[$k][1]=$v['msr_no'];
                $dt[$k][2]=$v['item'];
                $dt[$k][3]=$v['units'];                
                $dt[$k][4]=$v['groups'];
                $dt[$k][5]=$v['sgroup'];
                $dt[$k][6]=$v['qty'];
                $dt[$k][7]=$v['uom'];
                $dt[$k][8]=$v['priceunit'];
                $dt[$k][9]=$v['total'];
                $dt[$k][10]=$v['currency'];
                $dt[$k][11]=$v['import'];
                $dt[$k][12]=$v['costcenter'];
                $dt[$k][13]=$v['accsub_desc'];
                $dt[$k][14]='';
                $dt[$k][15]='';
                $dt[$k][16]='';
                $dt[$k][17]=$v['types'];
                $dt[$k][18]=$v['title'];
                $dt[$k][19]=$v['create_on'];
                $dt[$k][20]=$v['req_date'];                                
                $dt[$k][21]=$v['locations'];
                $dt[$k][22]=$v['method'];
                $dt[$k][23]=$v['dpoint_desc'];
                $dt[$k][24]=$v['requestfor_desc'];
                $dt[$k][25]=$v['deliveryterm_desc'];
                $dt[$k][26]=$v['inspection_desc'];
                $dt[$k][27]=$v['freight_desc'];                                
            }
        }
        return $this->output($dt);        
    }
}
?>