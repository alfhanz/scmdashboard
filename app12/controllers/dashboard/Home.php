<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Home extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('dashboard', TRUE);
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('dashboard/M_home', 'mhm');
        $this->load->model('vendor/M_all_intern', 'mai');
        $this->load->model('dashboard/m_dashboard');
        $cek = $this->mai->cek_session();
    }

    public function index($key = 'scm') {
        $menu_task = array();
        foreach ($this->db->where('PARENT', 0)->where('STATUS',1)->order_by('SORT', 'asc')->get('m_menu_dashboard')->result() as $r_group_menu_task) {
            
                $menu_task[$r_group_menu_task->ID_MENU] = array(
                    'ICON' => $r_group_menu_task->ICON,
                    'DESCRIPTION_IND' => $r_group_menu_task->DESCRIPTION_IND,
                    'DESCRIPTION_ENG' => $r_group_menu_task->DESCRIPTION_ENG,
                    'count' => 0,
                    'open_on_zero' => 0,
                    'menu_keys' => array(),
                    'menus' => array()
                );
                foreach ($this->db->where('PARENT', $r_group_menu_task->ID_MENU)->order_by('SORT', 'asc')->get('m_menu_dashboard')->result() as $r_sub_group_menu_task) {
                        $menu_task[$r_group_menu_task->ID_MENU]['menus'][$r_sub_group_menu_task->ID_MENU] = array(
                            'ICON' => $r_sub_group_menu_task->ICON,
                            'DESCRIPTION_IND' => $r_sub_group_menu_task->DESCRIPTION_IND,
                            'DESCRIPTION_ENG' => $r_sub_group_menu_task->DESCRIPTION_ENG,
                            'count' => 0,
                            'open_on_zero' => 0,
                            'menu_keys' => array(),
                            'menus' => array()
                        );

                        foreach ($this->db->where('PARENT', $r_sub_group_menu_task->ID_MENU)->order_by('SORT', 'asc')->get('m_menu_dashboard')->result() as $r_menu_task) {
                                
                                    $menu_task[$r_group_menu_task->ID_MENU]['menus'][$r_sub_group_menu_task->ID_MENU]['menus'][] = $r_menu_task;
                        }
                }
        }
            
        $data['menu_task'] = $menu_task;        

        // echopre($menu_task);exit;

        // echopre($data);exit;
        $this->template->display_dash('dashboard/V_home', $data);
    }
}