<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>ast11/css/custom/custom.css">
<script src="<?= base_url('ast11') ?>/js/plugins/highcharts/code/highcharts.js"></script>
<style type="text/css">
#cmms_grafik {
  width: 100%;
  /*max-width: 800px;*/
  height: 400px;
  margin: 0 auto
}
</style>
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title"><?= $title ?></h3>
      </div>
      <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home/">Home</a></li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-body">
      <section id="icon-tabs">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-scroll">
                  <form action="#" class="wizard-circle frm-bled" id="frm-bled" enctype="multipart/form-data">
                    <?php foreach ($y as $ye) {
                      $n = 0;
                      foreach ($data as $d) {
                        if($d->WASRST == $ye->wo_status)
                        {
                          $n =  $d->JML;
                        }
                      }
                    }?>
                    <fieldset>
                      <div class="col-md-12">
                        <div id="cmms_grafik"></div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
<script type="text/javascript">
Highcharts.chart('cmms_grafik', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Graph Data CMMS'
    },
    xAxis: {
        categories: [
        <?php foreach($y as $ye):?>
          '<?= $ye->wo_status_desc ?>',
        <?php endforeach;?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Data'
        }
    },
    tooltip: false,
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'CM',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 1) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'INS',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 2) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'MOD',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 3) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'OTG',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 4) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'CAP',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 5) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'PM',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 6) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'ADH',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 7) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: 'STA',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 8) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },{
        name: '3RD',
        data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == 9) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    }]
});
</script>