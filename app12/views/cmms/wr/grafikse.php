<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>ast11/css/custom/custom.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="<?= base_url('ast11') ?>/js/plugins/highcharts/code/highcharts.js"></script>
<style type="text/css">
#cmms_grafik {
  width: 100%;
  /*max-width: 800px;*/
  height: 500px;
  /*margin: 0 auto*/
}
</style>
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title"><?= $title ?></h3>
      </div>
      <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home/">Home</a></li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-body">
      <section id="icon-tabs">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-scroll">
                  <form action="#" class="wizard-circle frm-bled" id="frm-bled" enctype="multipart/form-data" method="post">
                    <fieldset>
                      <div class="form-group row">
                        <div class="col-md-2">
                          Creation Date (From - To)
                        </div>
                        <div class="col-md-3">
                          <input class="form-control dp" name="start_date" id="start_date" value="<?= $this->input->post('start_date'); ?>">
                        </div>
                        <div class="col-md-3">
                          <input class="form-control dp" name="end_date" id="end_date" value="<?= $this->input->post('end_date') ?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-2">
                          WO Type
                        </div>
                        <div class="col-md-6">
                          <select class="form-control select2" multiple="true" name="wo_type[]" id="wo_type">
                            <?php foreach ($wo_type as $r) {
                              echo "<option value='$r->id'>$r->code_alpha</option>";
                            }?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-2">
                          WO Status
                        </div>
                        <div class="col-md-6">
                          <select class="form-control select2" multiple="true" name="wo_status[]" id="wo_status">
                            <?php foreach ($wo_status as $r) {
                              echo "<option value='$r->wo_status'>$r->wo_status_desc</option>";
                            }?>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <button class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                      <div class="form-group row" style="margin-top: 10px">
                        <div class="col-md-12">
                          <div id="cmms_grafik"></div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.dp').datepicker({
    dateFormat : 'yy-mm-dd'
  });
  $('.select2').select2({
    placeholder: 'Search ',
    maximumSelectionLength: 9,
  });
})
<?php if (isset($data)): ?>
Highcharts.chart('cmms_grafik', {
    chart: {
        type: 'column'
    },
    title: {
        text: ' '
    },
    xAxis: {
        categories: [
        <?php foreach($y as $ye):?>
          '<?= $ye->wo_status_desc ?>',
        <?php endforeach;?>
        ],
        crosshair: true,
    labels: {
            x: -10
        }
    },
  plotOptions: {
    series: {
            dataLabels: {
                enabled: true
            }
        },
    column: {
            pointPadding: 0.2,
            borderWidth: 0,
            cursor: 'pointer',
            events: {
                click: function (event) {
                    console.log(event)
                    var wotype = event.point.series.name;
                    var wostatus = event.point.category;
                    var thelink = "<?= base_url('cmms/wo/index/wr') ?>?wotype="+wotype+"&wostatus="+wostatus;
                    window.open(thelink,"_blank");
                }
            }
        },
  },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Data'
        }
    },
    tooltip: false,
    series: [
    <?php foreach ($type as $t) : ?>
    {
      name: '<?= $t->code_alpha ?>',
      data: [<?php foreach ($y as $ye) {$n = 0;foreach ($data as $d) { if($d->WASRST == $ye->wo_status and $d->WATYPS == $t->id) { $n =  $d->JML; } }?><?=$n?>,<?php }?>]
    },
    <?php endforeach;?>
  ]
});
<?php endif;?>
</script>