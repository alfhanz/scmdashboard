<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>ast11/css/custom/custom.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="<?= base_url() ?>ast11/js/plugins/highcharts/code/highcharts.js"></script>
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title"><?= $title ?></h3>
      </div>
      <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home/">Home</a></li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-body">
      <section id="icon-tabs">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-scroll">
                  <form action="#" class="wizard-circle frm-bled" id="frm-bled" enctype="multipart/form-data" method="POST">
                    <fieldset>
                      <div class="form-group row hidden">
                        <div class="col-md-2">
              <label>Equipment *</label><br>
              <small>Max 12 Equipment</small>
            </div>
                        <div class="col-md-6">
                          <select multiple="true" class="form-control" name="search[]" id="search"></select>
                        </div>
            <div class="col-md-2"><button class="btn btn-primary">Submit</button></div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-12">
                          <div id="container"></div>
                        </div>
                        <div class="col-md-12 text-center">
                          <?php foreach ($rs as $k=>$d) :?>
                            <span style="margin-top:10px;background: <?=$d['color']?>" class="badge badge-info"><?= $k.'('.$d['desc'].')' ?></span>
                          <?php endforeach;?>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  <?php if(isset($rs)) : ?>
  var chart = Highcharts.chart('container', {

      title: {
          text: 'Equipment Repetitive Failure'
      },
  yAxis: {
        min: 0,
        title: {
            text: 'Work Order'
        }
    },
      xAxis: {
        
          categories: [<?php foreach ($rs as $key => $value) { ?> '<?= $key ?>', <?php } ?>]
      },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            }
        },
    column: {
            pointPadding: 0.2,
            borderWidth: 0,
            cursor: 'pointer',
            events: {
                click: function (event) {
          window.open("<?= base_url('cmms/wo/index/wr'); ?>?eq_no="+event.point.category,"_blank");
                }
            }
        },
    },
      series: [{
          type: 'column',
          colorByPoint: true,
          data: [<?php foreach ($rs as $key => $value) {?> {y: <?= $value['jml'] ?>,color: '<?=$value['color']?>'}, <?php } ?>],
          showInLegend: false
      }],
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">wo : </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true,
    enabled: false
    },
  });
  <?php endif;?>
  $('#search').select2({
    ajax: {
      delay:250,
      url: "<?= base_url('cmms/equipment/graph_search') ?>",
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term,
        }

        return query;
      },
      processResults: function (data) {
        return {
          results: data
        };
      }
    },
    placeholder: 'Search ',
    minimumInputLength: 2,
  maximumSelectionLength: 12,
  
  });
})
</script>