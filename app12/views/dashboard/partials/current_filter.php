<div id="current-filter" class="card">
    <div id="heading-current-filter" class="card-header d-flex col-md-10 checkbox-item-list" role="tab">
            <a data-toggle="collapse" id="curr" href="#accordion-current-filter" aria-expanded="false" aria-controls="accordion-current-filter" class="d-inline collapsed"><h4 class="card-title d-inline">Current Filter</h4>&nbsp; &nbsp;
            </a>
            <button id="btn_clps" class="btn btn-info btn-sm d-inline rounded" style="width: 30px;" data-toggle="collapse" data-target="#accordion-current-filter">✚</button>
    </div>
    <div role="tabpanel" id="accordion-current-filter" role="tabpanel" aria-labelledby="heading-current-filter" class="card-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="card-body">
            <div id="current-filter-content" class="col-md-11">
                Data not filtered
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#btn_clps').click(function(){
        $(this).text(function(i,old){
            return old=='✚' ?  '━' : '✚';
        });
    });
    $('#curr').click(function(){
        $('#btn_clps').text(function(i,old){
            return old=='✚' ?  '━' : '✚';
        });
    });
</script>