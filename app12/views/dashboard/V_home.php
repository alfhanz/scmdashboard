<script>
    $(document).ready(function() {
        $('.collapse').on('show.bs.collapse', function () {
            $(this).prev().css("height", "4em");
        });

        $('.collapse').on('hide.bs.collapse', function () {
            $(this).prev().css("height", "6em");
        });

    });
</script>
<style type="text/css">
    footer{
        margin-left: 0px !important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>ast11/css/custom/custom-home.css">
<div class="content-header row">
    <div class="content-header-left col-md-6 mb-1">
        <?php if (isset($title)) { ?>
            <h3 class="content-header-title"><?= @$title ?></h3>
        <?php } else { ?>
            <h3 class="content-header-title">Welcome to Supreme Energy Dashboard</h3>
        <?php } ?>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 mb-1">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <!-- <li class="breadcrumb-item"><a href="<?= base_url('dashboard/home_proc') ?>">Home</a></li> -->
            </ol>
        </div>
    </div>
</div>
<div class="content-body">
            <?php $colors=['primary','warning','danger','success']; ?>
            <?php $counter_color = 0 ?>
            <?php foreach ($menu_task as $group_menu) { ?>

                <?php if (count($group_menu['menus']) <> 0) { ?>
                    <h4 class="text-primary"><?= lang($group_menu['DESCRIPTION_IND'], $group_menu['DESCRIPTION_ENG']) ?></h4>
                    <div class="row">
                        <?php
                            $no = 0;
                            $row_type = 'odd';
                            $counter = 0;
                            $key = array();
                        ?>
                        <?php foreach ($group_menu['menus'] as $idx => $row) { ?>
                            <?php if (count($row['menus']) <> 0) { ?>

                                <div class="col-md-3">
                                    <a href="javascript:void(0)" class="subgroup-menu card" data-i="<?= $idx ?>">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media d-flex">
                                                    <div class="align-self-center">
                                                        <i class="<?= $row['ICON'] ?> <?= $colors[($counter_color%4)] ?> icons font-large-2 float-left"></i>
                                                    </div>
                                                    <div class="media-body text-right">
                                                        <span class="<?= $colors[($counter_color%4)] ?>"><?= lang($row['DESCRIPTION_IND'], $row['DESCRIPTION_ENG']) ?> <?= ($row['count'] <> 0 ) ? '<span class="badge badge-danger">'.$row['count'].'</span>' : '' ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php $key[] = $idx ?>
                                <?php $counter++ ?>
                                <?php $no++ ?>

                                <?php if ($counter == 4 || $no == count($group_menu['menus'])) {?>
                                    <?php foreach ($key as $k) { ?>
                                        <div data-i="<?= $k ?>" class="col-md-12 task-menu" style="display: none">
                                            <div class="row">
                                            <?php if(isset($group_menu['menus'][$k]['menus'])) { ?>
                                                <?php foreach ($group_menu['menus'][$k]['menus'] as $menu) { ?>
                                                    
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-content">
                                                                    <a href="<?= base_url($menu->URL) ?>" data-toggle="tooltip" data-placement="top" title="<?= $menu->DESCRIPTION_ENG ?>">
                                                                    <div class="media align-items-stretch">
                                                                        <div class="p-2 media-body menu-task-title">
                                                                            <?= lang($menu->DESCRIPTION_IND,  $menu->DESCRIPTION_ENG) ?>
                                                                        </div>
                                                                    </div>
                                                                    </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php $key = array() ?>
                                    <?php $counter = 0 ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <?php $counter_color++ ?>
                <?php } ?>
            <?php } ?>
</div>
<!-- <?php if (isset($dashboard_menu)) { ?>
    <div id="dashboard-menu" class="content-body">
        <div class="row">
            <?php
                $tamp=-1;
                $colors=['primary','warning','danger','success'];
                $count=0;
                if (isset($dashboard_menu[0])) {
                    foreach($dashboard_menu[0] as $k => $v) {
                        // echo'<div class="col-md-12 mb-1">
                        //     <h4><span class="IDN">'.$v["DESCRIPTION_IND"].'</span><span class="ENG">'.$v["DESCRIPTION_ENG"].'</span></h4>
                        // </div>';
                        echo '<div class="col-xl-3 col-lg-6 col-12 mx-auto mt-5">
                                    <a href="'.base_url($v["URL"]).'" class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media d-flex">
                                                    <div class="align-self-center">
                                                        <i class="'.$v["ICON"].' '.$colors[$count].' icons font-large-2 float-left"></i>
                                                    </div>
                                                    <div class="media-body text-right mt-1">
                                                        <span class="IDN">'.$v["DESCRIPTION_IND"].'</span><span class="ENG">'.$v["DESCRIPTION_ENG"].'</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>';
                                $count++;
                                if($count>3)
                                    $count=0;
                        
                    }
                }
            ?>
        </div>
    </div>
<?php } ?> -->



<script type="text/javascript">
    $(function() {
        $('.subgroup-menu').click(function() {
            if ($(this).hasClass('active')) {
                $('.subgroup-menu.active').removeClass('active');
            } else {
                $('.subgroup-menu.active').removeClass('active');
                $(this).addClass('active');
            }
            var idx = $(this).data('i');
            showTask(idx);
        });
    });
    function showTask(i) {
        if ($('.task-menu[data-i="'+i+'"]').hasClass('active')) {
            $('.task-menu').removeClass('active').slideUp(200);
        } else {
            $('.task-menu').removeClass('active').slideUp(200);
            setTimeout(function() {
                $('.task-menu[data-i="'+i+'"]').addClass('active').slideDown();
            }, 200);
        }
    }
</script>