<link rel="stylesheet" type="text/css" href="<?= base_url() ?>ast11/css/custom/custom.css">
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title"><?= lang("Retender History", "Retender History") ?></h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active"><?= lang("Pengadaan", "Procurement") ?></li>
                        <li class="breadcrumb-item active"><?= lang("Retender History", "Retender History") ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <table id="tbl" class="table table-striped table-bordered table-fixed-column order-column dataex-lr-fixedcolumns table-hover table-no-wrap display" width="100%">
                            <thead>
                              <tr>
                                <th width="1px">No</th>
                                <th>ED Number</th>
                                <th>Subject</th>
                                <th>Requestor Department</th>
                                <th>Procurement Specialist</th>
                                <th>Currency</th>
                                <th class="text-right">MSR Value</th>
                                <th>Closing Date</th>
                                <th class="text-center">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $no = 1;
                                foreach ($data->result() as $ed) :
                              ?>
                              <tr>
                                <td><?= $no ?></td>
                                <td><?= $ed->ed_no ?></td>
                                <td><?= $ed->subject ?></td>
                                <td><?= $ed->department ?></td>
                                <td><?= $ed->specialist ?></td>
                                <td><?= $ed->CURRENCY ?></td>
                                <td class="text-right">
                                  <?php
                                    if($ed->ee_value > 0) echo numIndo($ed->ee_value);
                                    else echo numIndo($ed->total_amount);
                                  ?>
                                </td>
                                <td data-sort="<?= $ed->closing_date ?>"><?= dateToIndo($ed->closing_date, false, true) ?></td>
                                <th class="text-center">
                                  <a href="<?= base_url('procurement/retender/show/'.$ed->msr_no) ?>?reteder=1" class="btn btn-primary btn-sm">Show</a>
                                </th>
                              </tr>
                              <?php $no++; ?>
                              <?php endforeach;?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>No</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Action</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#tbl tfoot th').each(function (i) {
      var title = $('#tbl thead th').eq($(this).index()).text();
      if ($(this).text() == 'No') {
        $(this).html('');
      } else if ($(this).text() == 'Action') {
        $(this).html('');
      } else {
        $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" data-index="' + i + '" />');
      }

    });
    var table = $('#tbl').DataTable({
      scrollX : true,
      fixedColumns: {
          leftColumns: 0,
          rightColumns: 1
      },
    });
    $(table.table().container()).on('keyup', 'tfoot input', function () {
      table.column($(this).data('index')).search(this.value).draw();
    });
  })
</script>