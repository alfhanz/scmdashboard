<?php 
	$retender = $this->approval_lib->retender($msr_no);
  // echo $this->db->last_query();
  // print_r($retender);
	if($retender) : 
?>
<div class="modal fade" id="modal-retender" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Retender Confirmation</h4>
      </div>
      <div class="modal-body">
        <form id="frm-retender" method="post" class="form-horizontal" enctype="multipart/form-data">
          <div class="form-group">
          	<p>Are you sure want to Retender this with Enquiry Document Number <?= str_replace('OR','OQ',$msr_no); ?> </p>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-success" onclick="retenderClick()">Yes Continue</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<a href="#" data-toggle="modal" data-target="#modal-retender" class="btn btn-danger" >Retender</a>
<script type="text/javascript">
	function retenderClick() {
		var form = $('#frm-retender')[0];
    var data = new FormData(form);
    $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url:"<?=base_url('approval/award/retender_action/'.$msr_no)?>",
      data: data,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      beforeSend:function(){
        start($('#icon-tabs'));
      },
      success:function(e){
       var r = eval("("+e+")");
        if(r.status){
          swal('Done','Retender Success','success');
          window.open("<?= base_url('home')?>", "_self")
        }
        else{
          swal('Fail',r.msg,'warning');

        }
        stop($('#icon-tabs')); 
      },
      error:function(){
        stop($('#icon-tabs'));
      }
  	});
	}
</script>
<?php endif;?>