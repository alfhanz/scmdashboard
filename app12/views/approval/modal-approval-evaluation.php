<a href="#" data-toggle="modal" data-target="#modal-approval-evalution" class="btn btn-sm btn-primary float-left" title="Approval Evalution List"><i class="fa fa-list"></i>  Approval Evaluation List</a>
<br>
<br>
<div class="modal fade" id="modal-approval-evalution" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modal Approval Evalution List</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="table-responsive">
		        <table class="table">
						  <thead>
						    <tr>
						      <th width="10">No</th>
						      <th style="width: 250px">Role</th>
						      <th>User</th>
						      <th>Transaction Date</th>
						    </tr>
						  </thead>
						  <tbody id="result-approval">
								<?php 
									$log_history = $this->M_approval->historyApprovalMsr($msr_no, ['technical evaluation','admin evaluation']);

									$evaluationApprovalList = [];
								  $evaluationApprovalList[] = ['name' => 'Administration evaluation', 'key' => 1, 'approval_name' => 'proc_specialist'];
									$evaluationApprovalList[] = ['name' => 'Approval Administration Evaluation', 'key' => 2, 'approval_name' => 'proc_head'];
									$evaluationApprovalList[] = ['name' => 'Technical Evaluation', 'key' => 3, 'approval_name' => 'creator_msr'];
									$evaluationApprovalList[] = ['name' => 'Approval Technical Evaluation', 'key' => 4, 'approval_name' => 'user_manager'];
									$evaluationApprovalList[] = ['name' => 'Acknowledge Administration Evaluation', 'key' => 5, 'approval_name' => 'creator_msr'];
									$evaluationApprovalList[] = ['name' => 'Acknowledge Administration Evaluation', 'key' => 6, 'approval_name' => 'user_manager'];
									$evaluationApprovalList[] = ['name' => 'Acknowledge Technical Evaluation', 'key' => 7, 'approval_name' => 'proc_specialist'];
									$evaluationApprovalList[] = ['name' => 'Acknowledge Technical Evaluation', 'key' => 8, 'approval_name' => 'proc_head'];
									$n = 1;

									$du['proc_specialist'] = $this->db->select('m_user.NAME proc_specialist')->join('m_user','m_user.ID_USER = t_assignment.user_id','left')->where(['msr_no'=>$msr_no])->get('t_assignment')->row_array();
									$du['proc_head'] = $this->db->select('m_user.NAME proc_head')->like('ROLES',','.assign_sp.',')->get('m_user')->row_array();
									$du['creator_msr'] = $this->db->select('m_user.NAME creator_msr, m_user.ID_USER')->join('m_user','m_user.ID_USER = t_msr.create_by','left')->where(['msr_no'=>$msr_no])->get('t_msr')->row_array();
									$du['user_manager'] = $this->db->query("select m_user.NAME user_manager from m_user where id_user = (SELECT t_jabatan.user_id FROM `t_jabatan` WHERE id = (SELECT parent_id FROM `t_jabatan` WHERE user_id = ".$du['creator_msr']['ID_USER']."))")->row_array();


									foreach($evaluationApprovalList as $r)
									{
										$key 	= $r['key'];
										$name 	= $r['name'];
										@$approvalName = $r['approval_name'];
										$user 	= '';

										@$approvalName = $du[$approvalName];
										@$approvalName = $approvalName[$r['approval_name']];

										$transction_date = '';
										foreach ($log_history->result() as $logs) {
											if($logs->description == $name)
											{
												$transction_date = dateToIndo($logs->created_at, false, true);
											}
										}

										echo "<tr><td>$n</td><td>$name</td><td>$approvalName</td><td>$transction_date</td></tr>";
										$n++;
									}
								?>
							</tbody>
						</table>
	  			</div>
      	</div>
      </div>
    </div>
  </div>
</div>