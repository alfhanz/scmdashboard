
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>ast11/css/custom/custom.css">
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title"><?= lang("Amendment Lists", "Amendment Lists") ?></h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active"><?= lang("Pengadaan", "Procurement") ?></li>
                        <li class="breadcrumb-item active"><?= lang("Amendment Lists", "Amendment Lists") ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <table id="tbl" class="table table-striped table-bordered table-fixed-column order-column dataex-lr-fixedcolumns table-hover table-no-wrap display" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Agreement No</th>
                                    <th>Amendment No</th>
                                    <th>Subject Work</th>
                                    <th>Company</th>
                                    <th>Currency</th>
                                    <th>Amendment Value</th>
                                    <th>Expiry Date</th>
                                    <th>Amendment Notification Date</th>
                                    <th>Amendment Date</th>
                                    <th>Contractor/Vendor Response Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                    foreach ($lists as $key => $value) {
                                      if($edit == 0)
                                      {
                                        $btnDetail = "<a href='".base_url('procurement/amendment_recommendation/amd_view/'.$value->id)."?amd=1' class='btn btn-success btn-sm'>Details</a>";
                                        $btnEdit = '';
                                      }
                                      if($edit == 1)
                                      {
                                        $btnEdit = "<a href='".base_url('procurement/amendment_recommendation/edit/'.$value->amd_reject_id)."' class='btn btn-info btn-sm'>Edit</a>";
                                        $btnDetail = '';
                                      }
                                      $amendment_date = $value->amendment_date ? dateToIndo($value->amendment_date) : null ;
                                      $amdValue = @$sub_total[$value->doc_no] ? numIndo($sub_total[$value->doc_no]) : '-';
                                      $expiryDate = getLastTimeAmd($value->doc_no, $value->amended_date,"<=");
                                      $expiryDate = $expiryDate ? dateToIndo($expiryDate) : '-';
                                        echo "<tr>
                                        <td>$no</td>
                                        <td>$value->po_no</td>
                                        <td>$value->doc_no</td>
                                        <td>$value->title</td>
                                        <td>$value->abbr</td>
                                        <td>$value->currency</td>
                                        <td align='right'>$amdValue</td>
                                        <td>$expiryDate</td>
                                        <td>".dateToIndo($value->notification_date)."</td>
                                        <td>$amendment_date</td>
                                        <td>".dateToIndo($value->responsed_at,false,true)."</td>
                                        <td>$btnDetail $btnEdit</td>
                                        </tr>";
                                        $no++;
                                    }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>No</th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th class="text-center">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#tbl tfoot th').each(function (i) {
      var title = $('#tbl thead th').eq($(this).index()).text();
      if ($(this).text() == 'No') {
        $(this).html('');
      } else if ($(this).text() == 'Action') {
        $(this).html('');
      } else {
        $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" data-index="' + i + '" />');
      }

    });
    var table = $('#tbl').DataTable({
      scrollX : true,
      fixedColumns: {
          leftColumns: 0,
          rightColumns: 1
      },
    });
    $(table.table().container()).on('keyup', 'tfoot input', function () {
      table.column($(this).data('index')).search(this.value).draw();
    });
  })
</script>