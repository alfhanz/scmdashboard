<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_home_inventory extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function menu($group = null, $menu='9') {
        if ($group) {
            $this->db->where('GROUP', $group)
                     ->where('ID_MENU', $menu)
                     ->or_where('PARENT', $menu);
        }
        $res = $this->db->select("*")
                ->from('m_menu_dashboard')
                ->where('STATUS','1')
                // ->where('PARENT', '1')
                ->order_by('SORT', 'ASC')
                ->get();
        if ($res->num_rows() != 0)
            return $res->result();
        else
            return false;
    }
}
?>