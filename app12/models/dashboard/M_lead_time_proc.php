<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_lead_time_proc extends CI_Model
{

    public function get_procurement_method_steps($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.po_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }

        $sql = $this->new_sql($filter);
        // echo($sql);exit;
        return $this->db->query($sql)->result();
    }

    public function get_procurement_method_steps_trend($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.po_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        
        // $sql = $this->old_sql_tren($condition_query);
        $sql = $this->new_sql_tren($filter);
        // echo($sql);exit;
        return $this->db->query($sql)->result();
    }
    public function new_sql($filter='')
    {
        $msr_filter = $this->msr_filter($filter);
        $assignment_filter = $this->assignment_filter($filter);
        $po_filter = $this->po_filter($filter);

        $sql = "SELECT 'MSR Release' AS description, 1 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days  
            FROM `t_dashboard` 
            join t_msr on t_dashboard.data_id = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            where module_code = 'msr_release' $msr_filter
            GROUP BY t_msr.id_pmethod
            UNION ALL
            SELECT 'MSR Verification' AS description, 2 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days  
            FROM `t_dashboard` 
            join t_msr on t_dashboard.data_id = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            where module_code = 'msr_verification' $msr_filter
            GROUP BY t_msr.id_pmethod
            UNION ALL
            SELECT 'MSR Assignment' AS description, 3 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days  
            FROM `t_dashboard` 
            join t_msr on t_dashboard.data_id = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            join t_assignment on t_assignment.msr_no = t_msr.msr_no
            where module_code = 'msr_assignment' $assignment_filter
            GROUP BY t_msr.id_pmethod
            UNION ALL
            SELECT 'ED Issuance' AS description, 4 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days  
            FROM `t_dashboard` 
            join t_msr on t_dashboard.data_id = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            join t_assignment on t_assignment.msr_no = t_msr.msr_no
            where module_code = 'ed_issuance' $assignment_filter
            GROUP BY t_msr.id_pmethod
            UNION ALL
            SELECT 'Evaluation' AS description, 5 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days  
            FROM `t_dashboard` 
            join t_msr on t_dashboard.data_id = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            join t_assignment on t_assignment.msr_no = t_msr.msr_no
            where module_code = 'bid_opening' $assignment_filter
            GROUP BY t_msr.id_pmethod
            UNION ALL
            SELECT 'Awarding' AS description, 6 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days  
            FROM `t_dashboard` 
            join t_msr on t_dashboard.data_id = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            join t_assignment on t_assignment.msr_no = t_msr.msr_no
            where module_code = 'awarding' $assignment_filter
            GROUP BY t_msr.id_pmethod
            union all
            SELECT 'Agreement' AS description, 7 AS step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(accept_completed_date,po_date)+1)) days  
            FROM `t_purchase_order` 
            join t_msr on t_purchase_order.msr_no = t_msr.msr_no
            join m_user on t_msr.create_by = m_user.ID_USER
            join t_assignment on t_assignment.msr_no = t_msr.msr_no
            where accept_completed = 1 $po_filter
            GROUP BY t_msr.id_pmethod";
        return $sql;
    }
    public function old_sql($condition_query='')
    {
        $sql = 'SELECT
            msr.description,
            step,
            id_pmethod,
            CEIL(
                AVG(
                    CASE WHEN COALESCE(days,0) = 0 THEN 1
                    ELSE days
                    END
                )
            ) days
        FROM (
            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'MSR Release\' AS description,
                1 AS step,
                DATEDIFF(t_approval.created_at, t_msr.create_on) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr\'
            AND t_approval.urutan = (
                SELECT MAX(a.urutan) FROM t_approval a
                JOIN m_approval b ON b.id = a.m_approval_id
                WHERE a.data_id = t_msr.msr_no
                AND b.module_kode = \'msr\'
            )

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'MSR Verification\' AS description,
                2 AS step,
                DATEDIFF(
                    t_approval.created_at, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr\'
                        ORDER BY a.urutan DESC
                        LIMIT 1
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = 1

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'MSR Assignment\' AS description,
                3 AS step,
                DATEDIFF(
                    t_approval.created_at, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr_spa\'
                        AND a.urutan = 1
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = 2

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'ED Issuance\' AS description,
                4 AS step,
                DATEDIFF(
                    t_approval.created_at, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr_spa\'
                        AND a.urutan = 2
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = (
                SELECT MAX(a.urutan) FROM t_approval a
                JOIN m_approval b ON b.id = a.m_approval_id
                WHERE a.data_id = t_msr.msr_no
                AND b.module_kode = \'msr_spa\'
            )

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'BID Opening\' AS description,
                5 AS step,
                DATEDIFF(
                    t_eq_data.bid_opening_date, (
                            SELECT a.created_at FROM t_approval a
                            JOIN m_approval b ON b.id = a.m_approval_id
                            WHERE a.data_id = t_msr.msr_no
                            AND b.module_kode = \'msr_spa\'
                            AND a.urutan = (
                            SELECT MAX(a.urutan) FROM t_approval a
                            JOIN m_approval b ON b.id = a.m_approval_id
                            WHERE a.data_id = t_msr.msr_no
                            AND b.module_kode = \'msr_spa\'
                        )
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_eq_data ON t_eq_data.msr_no = t_msr.msr_no

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'Awarding\' AS description,
                6 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.accept_award_date) FROM t_bl_detail a
                        WHERE a.msr_no = t_msr.msr_no
                    ), t_eq_data.bid_opening_date
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_eq_data ON t_eq_data.msr_no = t_msr.msr_no

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'Agreement\' AS description,
                7 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.issued_date) FROM t_purchase_order a
                        WHERE a.msr_no = t_msr.msr_no
                    ),
                    (
                        SELECT MAX(a.accept_award_date) FROM t_bl_detail a
                        WHERE a.msr_no = t_msr.msr_no
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'Procurement Lead Time\' AS description,
                8 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.issued_date) FROM t_purchase_order a
                        WHERE a.msr_no = t_msr.msr_no
                    ),
                    t_approval.created_at
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = 1

            UNION ALL

            SELECT DISTINCT
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'All Cycle\' AS description,
                9 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.issued_date) FROM t_purchase_order a
                        WHERE a.msr_no = t_msr.msr_no
                    ),
                    t_msr.create_on
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
        ) msr
        JOIN m_user ON m_user.ID_USER = msr.create_by
        JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
        '.$condition_query. '
        GROUP BY description, step, id_pmethod
        ORDER BY step ASC       ';
        return $sql;
    }
    public function msr_filter($filter='')
    {
        $condition = [];
        /*filter msr_release*/
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(start_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 't_msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 't_msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 't_msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = ' AND '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        return $condition_query;
    }
    public function assignment_filter($filter='')
    {
        $condition = [];
        /*filter msr_release*/
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(start_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 't_msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 't_msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 't_msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = ' AND '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        return $condition_query;
    }
    public function po_filter($filter='')
    {
        $condition = [];
        /*filter msr_release*/
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(accept_completed_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 't_msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 't_msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 't_msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = ' AND '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        return $condition_query;
    }

    public function new_sql_tren($filter='')
    {
        $msr_filter = $this->msr_filter($filter);
        $assignment_filter = $this->assignment_filter($filter);
        $po_filter = $this->po_filter($filter);

        $sql = "SELECT left(start_date,7) periode, 'MSR Release' AS description, 1 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days
        FROM `t_dashboard` 
        join t_msr on t_dashboard.data_id = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        where module_code = 'msr_release' $msr_filter
        group by left(start_date,7),t_msr.id_pmethod
        UNION ALL
        SELECT left(start_date,7) periode, 'MSR Verification' AS description, 2 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days
        FROM `t_dashboard` 
        join t_msr on t_dashboard.data_id = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        where module_code = 'msr_verification' $msr_filter
        group by left(start_date,7),t_msr.id_pmethod
        UNION ALL
        SELECT left(start_date,7) periode, 'MSR Assignment' AS description, 3 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days
        FROM `t_dashboard` 
        join t_msr on t_dashboard.data_id = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        join t_assignment on t_assignment.msr_no = t_msr.msr_no
        where module_code = 'msr_assignment' $assignment_filter
        group by left(start_date,7),t_msr.id_pmethod
        UNION ALL
        SELECT left(start_date,7) periode, 'ED Issuance' AS description, 4 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days
        FROM `t_dashboard` 
        join t_msr on t_dashboard.data_id = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        join t_assignment on t_assignment.msr_no = t_msr.msr_no
        where module_code = 'ed_issuance' $assignment_filter
        group by left(start_date,7),t_msr.id_pmethod
        UNION ALL
        SELECT left(start_date,7) periode, 'Evaluation' AS description, 5 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days
        FROM `t_dashboard` 
        join t_msr on t_dashboard.data_id = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        join t_assignment on t_assignment.msr_no = t_msr.msr_no
        where module_code = 'bid_opening' $assignment_filter
        group by left(start_date,7),t_msr.id_pmethod
        UNION ALL
        SELECT left(start_date,7) periode, 'Awarding' AS description, 6 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(end_date,start_date)+1)) days
        FROM `t_dashboard` 
        join t_msr on t_dashboard.data_id = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        join t_assignment on t_assignment.msr_no = t_msr.msr_no
        where module_code = 'awarding' $assignment_filter
        group by left(start_date,7),t_msr.id_pmethod
        union all
        SELECT left(accept_completed_date,7) periode, 'Agreement' AS description, 7 as step, t_msr.id_pmethod, CEIL(AVG(DATEDIFF(accept_completed_date,po_date)+1)) days
        FROM `t_purchase_order` 
        join t_msr on t_purchase_order.msr_no = t_msr.msr_no
        join m_user on t_msr.create_by = m_user.ID_USER
        join t_assignment on t_assignment.msr_no = t_msr.msr_no
        where accept_completed = 1 $po_filter
        group by left(accept_completed_date,7),t_msr.id_pmethod";
        return $sql;
    }

    public function old_sql_tren($condition_query='')
    {
        $sql = 'SELECT
            msr.periode,
            msr.description,
            step,
            id_pmethod,
            CEIL(
                AVG(
                    CASE WHEN COALESCE(days,0) = 0 THEN 1
                    ELSE days
                    END
                )
            ) days
        FROM (
            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'MSR Release\' AS description,
                1 AS step,
                DATEDIFF(t_approval.created_at, t_msr.create_on) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr\'
            AND t_approval.urutan = (
                SELECT MAX(a.urutan) FROM t_approval a
                JOIN m_approval b ON b.id = a.m_approval_id
                WHERE a.data_id = t_msr.msr_no
                AND b.module_kode = \'msr\'
            )

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'MSR Verification\' AS description,
                2 AS step,
                DATEDIFF(
                    t_approval.created_at, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr\'
                        ORDER BY a.urutan DESC
                        LIMIT 1
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = 1

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'MSR Assignment\' AS description,
                3 AS step,
                DATEDIFF(
                    t_approval.created_at, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr_spa\'
                        AND a.urutan = 1
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = 2

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'ED Issuance\' AS description,
                4 AS step,
                DATEDIFF(
                    t_approval.created_at, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr_spa\'
                        AND a.urutan = 2
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = (
                SELECT MAX(a.urutan) FROM t_approval a
                JOIN m_approval b ON b.id = a.m_approval_id
                WHERE a.data_id = t_msr.msr_no
                AND b.module_kode = \'msr_spa\'
            )

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'BID Opening\' AS description,
                5 AS step,
                DATEDIFF(
                    t_eq_data.bid_opening_date, (
                        SELECT a.created_at FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr_spa\'
                        AND a.urutan = (
                        SELECT MAX(a.urutan) FROM t_approval a
                        JOIN m_approval b ON b.id = a.m_approval_id
                        WHERE a.data_id = t_msr.msr_no
                        AND b.module_kode = \'msr_spa\'
                    )
                )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_eq_data ON t_eq_data.msr_no = t_msr.msr_no

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'Awarding\' AS description,
                6 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.accept_award_date) FROM t_bl_detail a
                        WHERE a.msr_no = t_msr.msr_no
                    ), t_eq_data.bid_opening_date
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_eq_data ON t_eq_data.msr_no = t_msr.msr_no

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'Agreement\' AS description,
                7 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.issued_date) FROM t_purchase_order a
                        WHERE a.msr_no = t_msr.msr_no
                    ),
                    (
                        SELECT MAX(a.accept_award_date) FROM t_bl_detail a
                        WHERE a.msr_no = t_msr.msr_no
                    )
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'Procurement Lead Time\' AS description,
                8 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.issued_date) FROM t_purchase_order a
                        WHERE a.msr_no = t_msr.msr_no
                    ),
                    t_approval.created_at
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
            JOIN t_approval ON t_approval.data_id = t_msr.msr_no
            JOIN m_approval ON m_approval.id = t_approval.m_approval_id
            WHERE m_approval.module_kode = \'msr_spa\'
            AND t_approval.urutan = 1

            UNION ALL

            SELECT DISTINCT
                LEFT(t_purchase_order.po_date, 7) AS periode,
                t_msr.*,
                t_purchase_order.po_no,
                t_purchase_order.po_type,
                t_purchase_order.po_date,
                \'All Cycle\' AS description,
                9 AS step,
                DATEDIFF(
                    (
                        SELECT MAX(a.issued_date) FROM t_purchase_order a
                        WHERE a.msr_no = t_msr.msr_no
                    ),
                    t_msr.create_on
                ) AS days
            FROM t_msr
            JOIN t_purchase_order ON t_purchase_order.msr_no = t_msr.msr_no AND t_purchase_order.issued = 1
        ) msr
        JOIN m_user ON m_user.ID_USER = msr.create_by
        JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
        '.$condition_query. '
        GROUP BY periode, description, step, id_pmethod
        ORDER BY step ASC';
        return $sql;
    }
}