<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_log_in extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function cek_intern($post) {
        $decrypt = $post['password'];
        $data = $this->db->select("*")
                ->from('m_user')
                ->where('USERNAME', $post['username'])
                ->where('PASSWORD', stripslashes(str_replace('/','_',crypt($decrypt, mykeyencrypt))))                
                ->where('STATUS', '1')
                ->get();
        return $data->row();
    }
	public function cek_intern_ex($post) {
        $data = $this->db->query("select * FROM m_user WHERE (USERNAME = '".$post['username']."' OR id_external = '".$post['username']."') AND is_external = '1' AND STATUS = '1'");
			//echo $this->db->last_query();exit;
        return $data->row();
    }

    public function get_menu_in($roles){
        $data = $this->db->select("MENU as M")
                ->from('m_user_roles')
                ->where_in('ID_USER_ROLES', $roles)
                ->where('STATUS', '1')
                ->get();
        return $data->result_array();
    }

    public function get_task_in($roles){
        $data = $this->db->select("TASK as M")
                ->from('m_user_roles')
                ->where_in('ID_USER_ROLES', $roles)
                ->where('STATUS', '1')
                ->get();
        return $data->result_array();
    }
}
?>