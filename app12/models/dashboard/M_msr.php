<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_msr extends CI_Model {
    public function auth() {
        $id_user = $this->session->userdata['ID_USER'];
        $user = $this->db->where('ID_USER', $id_user)
        ->get('m_user')
        ->row();
        return $user;
    }

    public function get_status($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        }else {
            $condition_query = '';
        }

        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        
        $sql = 'SELECT COUNT(1) AS number, SUM(value) AS value, status FROM (
            SELECT msr.*, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
					msr.freight_desc,
                    msr.status,
					msr.create_on
        ) msr
        GROUP BY status';
        // echo($sql); exit;
        return $this->db->query($sql)->result();
    }

    public function get_status_trend($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, periode, SUM(value) AS value, status FROM (
            SELECT msr.*, LEFT(req_date,7) as periode, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
					msr.freight_desc,
                    msr.create_on,
					msr.status,LEFT(req_date, 7)
        ) msr
        GROUP BY status, periode';
        // echo($sql);exit;
        return $this->db->query($sql)->result();
    }

    public function get_type($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, SUM(value) AS value, msr_type FROM (
            SELECT msr.*, m_msrtype.MSR_DESC as msr_type, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_msrtype on m_msrtype.ID_MSR = msr.id_msr_type
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
                    msr.freight_desc,
					msr.create_on,
					msr.status,m_msrtype.MSR_DESC
        ) msr
        GROUP BY msr_type';
        // echo($sql); exit;
        return $this->db->query($sql)->result();
    }

    public function get_type_trend($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, periode, SUM(value) AS value, msr_type FROM (
            SELECT msr.*, m_msrtype.MSR_DESC as msr_type, LEFT(req_date,7) as periode, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_msrtype on m_msrtype.ID_MSR = msr.id_msr_type
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
                    msr.freight_desc,
					msr.create_on,
					msr.status,LEFT(req_date, 7),m_msrtype.MSR_DESC
        ) msr
        GROUP BY msr_type, periode';
        // echo($sql); exit;
        return $this->db->query($sql)->result();
    }

    public function get_method($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, SUM(value) AS value, method FROM (
            SELECT msr.*, m_pmethod.PMETHOD_DESC as method, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_pmethod on m_pmethod.ID_PMETHOD = msr.id_pmethod
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
                    msr.freight_desc,
					msr.create_on,
					msr.status,m_pmethod.PMETHOD_DESC
        ) msr
        GROUP BY method';
        // echo($sql); exit;
        return $this->db->query($sql)->result();
    }

    public function get_method_trend($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, periode, SUM(value) AS value, method FROM (
            SELECT msr.*, m_pmethod.PMETHOD_DESC as method, LEFT(req_date,7) as periode, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_pmethod on m_pmethod.ID_PMETHOD = msr.id_pmethod
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
                    msr.freight_desc,
					msr.create_on,
					msr.status,LEFT(req_date, 7),m_pmethod.PMETHOD_DESC
        ) msr
        GROUP BY method, periode';
        // echo($sql);exit;
        return $this->db->query($sql)->result();
    }

    public function get_specialist($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, SUM(value) AS value, specialist FROM (
            SELECT msr.*,specialist.NAME as specialist, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            LEFT JOIN m_user specialist ON specialist.ID_USER = t_assignment.user_id
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
                    msr.freight_desc,
					msr.create_on,
					msr.status,specialist.NAME
        ) msr
        GROUP BY specialist';
        // echo($sql); exit;
        return $this->db->query($sql)->result();
    }

    public function get_specialist_trend($filter) {
        $condition = array();
        if (isset($filter['periode'])) {
            $condition[] = 'LEFT(msr.req_date, 7) IN (\''.implode('\',\'', $filter['periode']).'\')';
        }
        if (isset($filter['company'])) {
            $condition[] = 'msr.id_company IN (\''.implode('\',\'', $filter['company']).'\')';
        }
        if (isset($filter['department'])) {
            $condition[] = 'm_user.ID_DEPARTMENT IN (\''.implode('\',\'', $filter['department']).'\')';
        }
        if (isset($filter['status'])) {
            $condition[] = 'msr.status IN (\''.implode('\',\'', $filter['status']).'\')';
        }
        if (isset($filter['type'])) {
            $condition[] = 'msr.id_msr_type IN (\''.implode('\',\'', $filter['type']).'\')';
        }
        if (isset($filter['method'])) {
            $condition[] = 'msr.id_pmethod IN (\''.implode('\',\'', $filter['method']).'\')';
        }
        if (isset($filter['specialist'])) {
            $condition[] = 't_assignment.user_id IN (\''.implode('\',\'', $filter['specialist']).'\')';
        }
        if (count($condition) <> 0) {
            $condition_query = 'WHERE '.implode(' AND ', $condition);
        } else {
            $condition_query = '';
        }
        $user = $this->auth();
        $condition_filt = '';
        $comp = explode(',', $user->COMPANY);
        $company = '\''.implode('\',\'', $comp).'\'';
        $dept = explode(',', $user->ID_DEPARTMENT);
        $department = '\''.implode('\',\'', $dept).'\'';
        // role 18 adalah BOD dimana hanya bisa melihat pada company dan deparment sendiri dan department 101013800 adalah SCM yang memiliki otorisasi melihat semuanya
        $role = '18';
        if(stripos($user->ROLES, $role ) !== FALSE OR stripos($user->ID_DEPARTMENT,'101013800') !== TRUE){
            $condition_filt = ' AND msr.id_company IN('.$company.') AND msr.id_department IN ('.$department.')';
        }
        if(stripos($user->ID_DEPARTMENT,'101013800') !== FALSE){
            $condition_filt='';
        }
        $sql = 'SELECT COUNT(1) AS number, periode, SUM(value) AS value, specialist FROM (
            SELECT msr.*, specialist.NAME as specialist, LEFT(req_date,7) as periode, SUM(total_amount_base) AS value FROM (
                SELECT * FROM v_msr
            ) msr
            JOIN m_user ON m_user.ID_USER = msr.create_by
            LEFT JOIN t_assignment ON t_assignment.msr_no = msr.msr_no
            LEFT JOIN m_user specialist ON specialist.ID_USER = t_assignment.user_id
            '.$condition_query.'
            '.$condition_filt.'
            GROUP BY msr.msr_no,
					msr.id_company,
					msr.company_desc,
					msr.id_department,
					msr.department_desc,
					msr.id_msr_type,
					msr.title,
					msr.id_pmethod,
					msr.create_by,
					msr.total_amount_base,
					msr.req_date,
					msr.importation_desc,
					msr.costcenter_desc,
					msr.id_currency_base,
					msr.rloc_desc,
					msr.dpoint_desc,
					msr.requestfor_desc,
					msr.deliveryterm_desc,
					msr.inspection_desc,
                    msr.freight_desc,
					msr.create_on,
					msr.status,LEFT(req_date, 7),specialist.NAME
        ) msr
        GROUP BY specialist, periode';
        // echo($sql);exit;
        return $this->db->query($sql)->result();
    }
}