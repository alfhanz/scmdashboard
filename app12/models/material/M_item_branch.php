<?php if (!defined('BASEPATH')) exit('Anda tidak masuk dengan benar');

class M_item_branch extends CI_Model {

	public function __construct() {
    parent::__construct();
    $this->jde = $this->load->database('oracle', true);
    $this->table_item_branch = 'F4101Z1'; //oracle
  }

  public function keyFieldAndUpdate()
  {
    /*this tested*/
    $F0002 = jdeDevProdTable('F0002');
    $r = $this->jde->query("SELECT NNN002,NNN001 FROM $F0002 WHERE NNSY='47'")->row();
    $q = $this->jde->query("UPDATE $F0002 SET NNN002=NNN002+1, NNN001=NNN001+1 WHERE NNSY='47'");
    /*SZEDBT*/
    $rs['SZEDBT']    = $r->NNN002;
    $rs['SZEDTN']    = $r->NNN001;
    return $rs;
  }

  public function item_branch_preparation($res='', $addRes='')
  {
	  $this->jde->trans_begin();
	  
    $keyFieldAndUpdate = $this->keyFieldAndUpdate();
    $SZEDBT   = $keyFieldAndUpdate['SZEDBT'];
    $SZEDTN   = $keyFieldAndUpdate['SZEDTN'];
    $lineItem = 1000;

    $itemBranch = explode(',', $res[0]->ITEM_BRANCH);
		$dataItemBranch =[];

    foreach ($itemBranch as $key => $value) {
      $dataItemBranch = [
        'SZEDLN'  => $lineItem,
        'SZEDTN'  => $SZEDTN,
        'SZEDBT'  => $SZEDBT,
        'SZMCU'   => trim($value),
    		'SZITM'   => $res[0]->MATERIAL,
        'SZLITM'  => $res[0]->MATERIAL_CODE,
        'SZDSC1'  => $res[0]->MATERIAL_NAME,
        'SZDSC2'  => $res[0]->SHORTDESC,
        'SZSRTX'  => $res[0]->SEARCH_TEXT,
        'SZSRP6'  => $addRes[0]->PARENT, 
        'SZSRP7'  => $addRes[0]->INVENTORY_TYPE, 
        'SZSRP8'  => $addRes[0]->CRITICALITY,
        'SZSRP9'  => $addRes[0]->PROJECT_PHASE,
        'SZSRP0'  => $addRes[0]->MAIN_GROUP,
        'SZUOM1'  => $res[0]->UOM,
        'SZUOM2'  => $res[0]->UOM,
        'SZUOM3'  => $res[0]->UOM,
		'SZGLPT'  => $res[0]->glClassCode,
        
      ];
      $this->insertJdeItemBranch($dataItemBranch);
      $lineItem = $lineItem+1000;
    }
	
	if($this->jde->trans_status() === true)
	{
		$this->jde->trans_commit();
	}
	else
	{
		$this->jde->trans_rollback();
	}
    return $dataItemBranch;
  }

  public function qRequestJDEInsertMaterial($req_no='')
  {
		$m_mat_header = $this->db->query("SELECT g.code as glClassCode,m.*, l.code as LINE_TYPE_CODE, s.code as STOCKING_TYPE_NAME FROM m_material m join m_gl_class g on g.id=m.gl_class join m_line_type l ON l.id=m.LINE_TYPE join m_stocking_type s ON s.id=m.STOCKING_TYPE where m.MATERIAL='".$req_no."'");

    return $m_mat_header->result();
  }
  public function qRequestJDEInsertMaterialCategory($req_no='')
  {
  	$query_select_mat = $this->db->query("SELECT g.PARENT,LEFT(m.MATERIAL_CODE,2) as MAIN_GROUP,m.* FROM m_material m JOIN m_material_group g on g.MATERIAL_GROUP=CAST(LEFT(m.MATERIAL_CODE,2) as SIGNED) AND g.TYPE='GOODS' where MATERIAL='".$req_no."' ");
		return $query_select_mat->result();
  }
  public function insertJdeItemBranch($res='')
    {
      $this->jde->insert($this->table_item_branch, [
      'SZEDUS'  => 'BSV01', 'SZEDBT'  => $res['SZEDBT'], 'SZEDTN'  => $res['SZEDTN'], 
      'SZEDLN'  => $res['SZEDLN'], 'SZEDCT'  => ' ', 'SZTYTN'  => ' ',
      'SZEDFT'  => ' ', 'SZEDDT'  => 0, 'SZDRIN'  => ' ', 'SZTFLA' => ' ',
      'SZEDDL'  => 0, 'SZEDSP'  => ' ', 'SZPNID'  => ' ', 'SZTNAC'  => 'A',
      'SZITBR'  => '2', 'SZITM'   => $res['SZITM'], 'SZKIT'   => 0, 'SZLITM'  => $res['SZLITM'],
      'SZAITM'  => ' ', 'SZDSC1'  => $res['SZDSC1'], 'SZDSC2'  => $res['SZDSC2'],
      'SZSRTX'  => $res['SZSRTX'], 'SZALN'   => ' ', 'SZSRP1'  => ' ', 'SZSRP2'  => ' ',
      'SZSRP3'  => ' ', 'SZSRP4'  => ' ', 'SZSRP5'  => ' ', 'SZSRP6'  => $res['SZSRP6'],
      'SZSRP7'  => $res['SZSRP7'], 'SZSRP8'  => $res['SZSRP8'], 'SZSRP9'  => $res['SZSRP9'],
      'SZSRP0'  => $res['SZSRP0'], 'SZPRP1'  => ' ', 'SZPRP2'  => ' ', 'SZPRP3'  => ' ',
      'SZPRP4'  => ' ', 'SZPRP5'  => ' ', 'SZPRP6'  => ' ', 'SZPRP7'  => ' ', 'SZPRP8'  => ' ',
      'SZPRP9'  => ' ', 'SZPRP0'  => ' ', 'SZCDCD'  => ' ', 'SZPDGR'  => ' ', 'SZDSGP'  => ' ',
      'SZPRGR'  => ' ', 'SZRPRC'  => ' ', 'SZORPR'  => ' ', 'SZBUYR'  => 0, 'SZDRAW'  => ' ',
      'SZRVNO'  => ' ', 'SZDSZE'  => ' ', 'SZVCUD'  => 0, 'SZCARS'  => 0, 'SZCARP'  => 0,
      'SZSHCN'  => ' ', 'SZSHCM'  => ' ', 'SZUOM1'  => $res['SZUOM1'], 'SZUOM2'  => $res['SZUOM2'],
      'SZUOM3'  => $res['SZUOM3'], 'SZUOM4'  => ' ', 'SZUOM6'  => ' ', 'SZUOM8'  => ' ',
      'SZUOM9'  => ' ', 'SZUWUM'  => ' ', 'SZUVM1'  => ' ', 'SZSUTM'  => ' ', 'SZUMVW'  => ' ',
      'SZCYCL'  => ' ', 'SZGLPT'  => $res['SZGLPT'], 'SZPLEV'  => ' ', 'SZPPLV'  => ' ',
      'SZCLEV'  => '2', 'SZPRPO'  => ' ', 'SZCKAV'  => ' ', 'SZBPFG'  => ' ', 'SZSRCE'  => ' ',
      'SZOT1Y'  => ' ', 'SZOT2Y'  => ' ', 'SZSTDP'  => 0, 'SZFRMP'  => 0, 'SZTHRP'  => 0,
      'SZSTDG'  => ' ', 'SZFRGD'  => ' ', 'SZTHGD'  => ' ', 'SZCOTY'  => ' ', 'SZSTKT'  => 'S',
      'SZLNTY'  => 'S', 'SZCONT'  => ' ', 'SZBACK'  => ' ', 'SZIFLA'  => ' ', 'SZINMG'  => ' ',
      'SZABCS'  => ' ', 'SZABCM'  => ' ', 'SZABCI'  => ' ', 'SZOVR'   => ' ', 'SZWARR'  => ' ',
      'SZCMCG'  => ' ', 'SZSRNR'  => ' ', 'SZPMTH'  => ' ', 'SZFIFO'  => ' ', 'SZLOTS'  => ' ',
      'SZSLD'   => 0, 'SZANPL'  => 0, 'SZMPST'  => ' ', 'SZPCTM'  => 0, 'SZMMPC'  => 0,
      'SZPTSC'  => ' ', 'SZSNS'   => ' ', 'SZLTLV'  => 0, 'SZLTMF'  => 0, 'SZLTCM'  => 0,
      'SZOPC'   => ' ', 'SZOPV'   => 0, 'SZACQ'   => 0, 'SZMLQ'   => 0, 'SZLTPU'  => 0,
      'SZMPSP'  => ' ', 'SZMRPP'  => ' ', 'SZITC'   => ' ', 'SZORDW'  => ' ', 'SZMTF1'  => 0,
      'SZMTF2'  => 0, 'SZMTF3'  => 0, 'SZMTF4'  => 0, 'SZMTF5'  => 0, 'SZEXPD'  => 0,
      'SZDEFD'  => 0, 'SZSFLT'  => 0, 'SZMAKE'  => ' ', 'SZCOBY'  => ' ', 'SZLLX'   => 0,
      'SZCMGL'  => ' ', 'SZCOMH'  => 0, 'SZAVRT'  => 0, 'SZUPCN'  => ' ', 'SZSCC0'  => ' ',
      'SZUMDF'  => ' ', 'SZUMS0'  => ' ', 'SZUMUP'  => ' ', 'SZUMS1'  => ' ', 'SZUMS2'  => ' ',
      'SZUMS3'  => ' ', 'SZUMS4'  => ' ', 'SZUMS5'  => ' ', 'SZUMS6'  => ' ', 'SZUMS7'  => ' ', 'SZUMS8'  => ' ',
      'SZWTRQ'  => ' ', 'SZEQTY'  => ' ', 'SZPOC'   => ' ', 'SZMCU'   => "   $res[SZMCU]", 'SZMMCU'  => ' ',
      'SZVEND'  => 0, 'SZORIG'  => ' ', 'SZROPI'  => 0, 'SZROQI'  => 0, 'SZRQMX'  => 0, 'SZRQMN'  => 0,
      'SZWOMO'  => 0, 'SZSERV'  => 0, 'SZSAFE'  => 0, 'SZFUF1'  => ' ', 'SZTX'    => ' ', 'SZTAX1'  => ' ',
      'SZMRPD'  => ' ', 'SZMRPC'  => ' ', 'SZUPC'   => 0, 'SZMERL'  => ' ', 'SZECO'   => ' ', 'SZECTY'  => ' ',
      'SZECOD'  => 0, 'SZMOVD'  => 0, 'SZQUED'  => 0, 'SZSETL'  => 0, 'SZSRNK'  => 0, 'SZSRKF'  => ' ',
      'SZTIMB'  => ' ', 'SZBQTY'  => 0, 'SZMULT'  => 0, 'SZLFDJ'  => 0, 'SZMLOT'  => ' ', 'SZLOCN'  => ' ',
      'SZLOTN'  => ' ', 'SZURCD'  => ' ', 'SZURDT'  => 0, 'SZURAT'  => 0, 'SZURAB'  => 0,
      'SZURRF'  => ' ',          'SZTORG' => ' ',         'SZSTAW'  => ' ',                'SZEFFT' => 0,                'SZDOC'   => '4361',
      'SZDCT'   => ' ',          'SZUSER' => 'EKO075',   'SZPID'   => 'ER4101Z1I',       'SZJOBN' => 'JDE-DEV01 ',     'SZUPMJ'  => '119311',
      'SZTDAY'  => '202627',    'SZTMPL' => ' ',         'SZSEG1'  => ' ',                'SZSEG2' => ' ',               'SZSEG3'  => ' ',
      'SZSEG4'  => ' ',          'SZSEG5' => ' ',         'SZSEG6'  => ' ',                'SZSEG7' => ' ',               'SZSEG8'  => ' ',
      'SZSEG9'  => ' ',          'SZSEG0' => ' ',         'SZAING'  => '0',               'SZCMDM' => '1',              'SZDPPO'  => ' ',
      'SZDUAL'  => ' ',          'SZLECM' => '1',        'SZMIC'   => ' ',                'SZXDCK' => ' ',               'SZDLTL'  => 0,
      'SZLEDD'  => 0,           'SZPEFD' => 0,          'SZSBDD'  => 0,                 'SZU1DD' => 0,                'SZU2DD'  => 0,
      'SZU3DD'  => 0,           'SZU4DD' => 0,          'SZU5DD'  => 0,                 'SZBBDD' => 0,                'SZLOTC'  => ' ',
      'SZEXPI'  => '1',         'SZCONB' => ' ',         'SZPRI1'  => 0,                 'SZPRI2' => 0,                'SZGCMP'  => ' ',
      'SZAPSC'  => ' ',          'SZCMETH'=> ' ',         'SZLAF'=> ' ',                   'SZLTFM'=> ' ',                'SZRWLA'=> ' ',
      'SZLNPA'  => ' ',          'SZASHL'=> ' ',          'SZVMINV'=> ' ',                 'SZLTCV'=> 0,                 'SZOPTH'=> 0,
      'SZCUTH'  => 0,           'SZUMTH'=> ' ',          'SZLMFG'=> ' ',                  'SZLINE'=> ' ',                'SZDFTPCT'=> 0,
      'SZKBIT'  => ' ',          'SZDFENDITM' => ' ',     'SZKANEXLL' => ' ',              'SZSCPSELL'=> 0,              'SZMOPTH'=> 0,
      'SZMCUTH' => 0,           'SZCUMTH'=> ' ',         'SZATPRN'=> ' ',                'SZATPCA'=> ' ',               'SZATPAC'=> ' ',  
      'SZCOORE' => 0,           'SZVCPFC'=> ' ',         'SZPNYN'=> 'N' 
    ]);
    }
}