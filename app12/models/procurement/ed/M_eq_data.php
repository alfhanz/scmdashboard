<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_eq_data extends M_base {

    protected $table = 't_eq_data';

    public function view_ed() {
        $this->db->select('t_eq_data.*, t_bl.bled_no, t_msr.total_amount, t_msr.id_company, t_msr.create_by as id_requestor, m_user.ID_DEPARTMENT, m_company.DESCRIPTION as company, m_departement.DEPARTMENT_DESC as department, m_user.NAME as requestor, m_currency.CURRENCY as currency_desc, t_msr.id_currency_base as currency_base, currency_base.CURRENCY as currency_base_desc')
        ->join('t_bl', 't_bl.msr_no = t_eq_data.msr_no')
        ->join('t_msr', 't_msr.msr_no = t_eq_data.msr_no')
        ->join('m_company', 'm_company.ID_COMPANY = t_msr.id_company')
        ->join('m_user', 'm_user.ID_USER = t_msr.create_by')
        ->join('m_departement', 'm_departement.ID_DEPARTMENT = m_user.ID_DEPARTMENT')
        ->join('t_assignment', 't_assignment.msr_no = t_eq_data.msr_no')
        ->join('m_currency', 'm_currency.ID = t_eq_data.currency')
        ->join('m_currency currency_base', 'currency_base.ID = t_msr.id_currency_base');
    }

    public function scope_negotiation() {
        $this->db->where('t_eq_data.administrative', 5)
        ->where('t_eq_data.technical', 5)
        ->where('t_eq_data.commercial', 0)
        ->where('t_eq_data.award', 0);
    }

    public function scope_procurement_specialist() {
        $this->db->where('t_assignment.user_id', $this->session->userdata('ID_USER'));
    }
    public function scope_time_edit_sop()
    {
        $this->db->where([
            't_eq_data.bid_opening' => 1,
            'administrative' => ' <= 4',
            'technical' => ' <= 4',
        ]);
    }
	public function scope_retender()
    {
        $this->db->where([
            'technical' => 2
        ]);
    }
    /*retender*/
    public function retender($msr_no='')
    {
      $this->db->trans_begin();
      /*t_eq_data*/
      $this->db->query("insert into trash_t_eq_data select * from t_eq_data where msr_no = '$msr_no'");
      $this->db->where('msr_no', $msr_no)->delete($this->table);

      /*t_approval*/
      $where = " data_id = '$msr_no' and m_approval_id in (select id from m_approval where module_kode in('msr_spa'))";
      $this->db->query("update t_approval set status = 0, deskripsi = null where $where ");
      $this->db->query("update t_approval set status = 4 where m_approval_id = 8 and data_id = '$msr_no'");
      $this->db->query("update t_approval set status = 1 where m_approval_id = 7 and data_id = '$msr_no'");
      
      $where = "data_id = '$msr_no' and m_approval_id in (select id from m_approval where module_kode in('award')";
      $this->db->query("insert into trash_t_approval select * from t_approval where $where)");

      $this->db->query("delete from t_approval where $where)");
      
      /*log_history*/
      $where = "data_id = '$msr_no' and module_kode not in ('msr')";
      $this->db->query("insert into trash_log_history select * from log_history where $where");

      $this->db->query("delete from log_history where $where");

      
      $this->retenderblsop($msr_no);

      if($this->db->trans_status() === true)
      {
        $this->db->trans_commit();
        // $this->db->trans_rollback();
        return true;
      }
      else
      {
        $this->db->trans_rollback();
        return false;
      }
    }

    public function retenderblsop($msr_no='')
    {
      /*t_bl*/
      $bled_no = str_replace('OR','OQ',$msr_no);
      $this->db->query("insert into trash_t_bl select * from t_bl where bled_no = '$bled_no'");
      $this->db->where('bled_no', $bled_no)->delete('t_bl');

      /*[t_bl_detail, trash_t_bl_detail]*/
      $tableList = [
        't_bl_detail'=>'trash_t_bl_detail',
        't_sop'=>'trash_t_sop',
        't_sop_bid'=>'trash_t_sop_bid',
        't_nego'=>'trash_t_nego',
        't_nego_detail'=>'trash_t_nego_detail',
      ];
      foreach ($tableList as $key => $value) {
        $this->db->query("insert into $value select * from $key where msr_no = '$msr_no'");
        $this->db->where('msr_no', $msr_no)->delete($key);
      }
    }

    public function retender_reseverse($msr_no='')
    {
      # code...
    }
}