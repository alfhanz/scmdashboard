<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class T_arf_recommendation_preparation extends CI_Model {
	public function __construct() {
      parent::__construct();
      $this->db = $this->load->database('default', true);
      $this->table = 't_arf_recommendation_preparation';
  }
  public function view($id='')
  {
  	$r = $this->db->where(['arf_response_id'=>$id])->get($this->table)->row();
  	$data['new_date_1'] = $r ? dateToIndo($r->new_date_1) : '';
    $data['new_date_2'] = $r ? dateToIndo($r->new_date_2) : '';
    $data['remarks_1'] = $r ? $r->remarks_1 : '';
    $data['remarks_2'] = $r ? $r->remarks_2 : '';
    $data['extend1'] = $r ? $r->extend1 : '';
  	$data['extend2'] = $r ? $r->extend2 : '';
    $data['recom'] = $r;
  	return $data;
  }
  public function nego_lists($arf_response_id)
  {
    $sql = "select t_arf_nego.* from 
    t_arf_response
    join t_arf_nego on t_arf_response.id = t_arf_nego.arf_response_id
    where t_arf_response.id = $arf_response_id";
    $arfNegos = $this->db->query($sql)->result();
    $sql = "select t_arf_nego_detail.*, t_arf_sop.item, (case when t_arf_sop.qty2 > 0 then qty2*qty1 else qty1 end) qty, (case when uom2 != '' then concat(uom1,' & ',uom2) else uom1 end) uom from 
    t_arf_response
    left join t_arf_nego on t_arf_response.id = t_arf_nego.arf_response_id
    left join t_arf_nego_detail on t_arf_nego_detail.arf_nego_id = t_arf_nego.id and t_arf_response.id = t_arf_nego_detail.arf_response_id
    left join t_arf_sop on t_arf_sop.id = t_arf_nego_detail.arf_sop_id
    where t_arf_response.id = $arf_response_id";
    $arfNegoDetails = $this->db->query($sql)->result();
    return ['arfNegos'=>$arfNegos, 'arfNegoDetails'=>$arfNegoDetails];
  }
  public function find($id='')
  {
    return $this->db->where('id', $id)->get($this->table)->row();
  }
  public function sumAllAmdDetail($poNo='')
  {
    $q = "select  sum(sub_total) total from (SELECT
      (case when `t_arf_response_detail`.`qty2` is not null or `t_arf_response_detail`.`qty2` > 0 then `t_arf_response_detail`.`qty2` * `t_arf_response_detail`.`qty1` else `t_arf_response_detail`.`qty1` end) res_qty,
      `t_arf_response_detail`.`unit_price` AS `res_unit_price`,
      `t_arf_response_detail`.`unit_price_base` AS `res_unit_price_base`,
      (case when `t_arf_nego_detail`.`unit_price` > 0 then `t_arf_nego_detail`.`unit_price` else `t_arf_response_detail`.`unit_price` end) pricing,
      (case when `t_arf_nego_detail`.`unit_price` > 0 then `t_arf_nego_detail`.`unit_price` else `t_arf_response_detail`.`unit_price` end) * 
      (case when `t_arf_response_detail`.`qty2` is not null or `t_arf_response_detail`.`qty2` > 0 then `t_arf_response_detail`.`qty2` * `t_arf_response_detail`.`qty1` else `t_arf_response_detail`.`qty1` end) sub_total
    FROM
      `t_arf_sop`
      LEFT JOIN `t_arf_response_detail` ON `t_arf_response_detail`.`detail_id` = `t_arf_sop`.`id`
      LEFT JOIN `t_arf_detail` ON `t_arf_detail`.`id` = `t_arf_sop`.`arf_item_id`
      LEFT JOIN `t_arf` ON `t_arf`.`id` = `t_arf_detail`.`doc_id`
      LEFT JOIN `m_itemtype` ON `m_itemtype`.`ID_ITEMTYPE` = `t_arf_sop`.`id_itemtype`
      LEFT JOIN `m_msr_inventory_type` ON `m_msr_inventory_type`.`id` = `t_arf_sop`.`inv_type`
      LEFT JOIN `t_purchase_order` ON `t_purchase_order`.`po_no` = `t_arf`.`po_no`
      LEFT JOIN `t_purchase_order_detail` ON `t_purchase_order_detail`.`po_id` = `t_purchase_order`.`id` 
      AND `t_purchase_order_detail`.`semic_no` = `t_arf_sop`.`item_semic_no_value` 
      AND `t_purchase_order_detail`.`material_desc` = `t_arf_sop`.`item`
      LEFT JOIN `m_material` ON `m_material`.`MATERIAL_CODE` = `t_arf_sop`.`item_semic_no_value`
      LEFT JOIN `m_gl_class` ON `m_gl_class`.`id` = `m_material`.`GL_CLASS`
      LEFT JOIN `m_line_type` ON `m_line_type`.`id` = `m_material`.`LINE_TYPE`
      LEFT JOIN `t_arf_response` ON `t_arf_response`.`doc_no` = `t_arf_response_detail`.`doc_no`
      LEFT JOIN ( SELECT * FROM t_arf_nego WHERE STATUS = 2 ORDER BY id DESC LIMIT 1 ) t_arf_nego ON `t_arf_nego`.`arf_response_id` = `t_arf_response`.`id`
      LEFT JOIN `t_arf_nego_detail` ON `t_arf_sop`.`id` = `t_arf_nego_detail`.`arf_sop_id` 
      AND `t_arf_nego`.`id` = `t_arf_nego_detail`.`arf_nego_id` 
    WHERE
      `t_arf`.`po_no` = '$poNo') a";
      return $this->db->query($q)->row();
  }
}