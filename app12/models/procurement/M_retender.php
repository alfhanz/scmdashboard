<?php if (!defined('BASEPATH')) exit('Anda tidak masuk dengan benar');

class M_retender extends CI_Model {

  public function __construct() {
      parent::__construct();
      $this->tbmsr  = 't_msr';
      $this->tbmi   = 't_msr_item';
      $this->tbl    = 't_bl';
      $this->tbl_trash    = 'trash_t_bl';
      $this->tbld   = 't_bl_detail';
      $this->tbld_trash   = 'trash_t_bl_detail';
      $this->tbeq   = 't_eq_data';
      $this->tbeq_trash   = 'trash_t_eq_data';
      $this->tbv    = 'm_vendor';
      $this->tbdd   = 't_bid_detail';
      $this->tbloi  = 't_letter_of_intent';
      $this->tbpo   = 't_purchase_order';
      $this->tbtapp = 't_approval';
      $this->tsop   = 't_sop';
      $this->trashtsop   = 'trash_t_sop';
      $this->mc     = 'm_currency';
      $this->mdt     = 'm_deliveryterm';
      $this->mdp     = 'm_deliverypoint';
      $this->msrinv     = 'm_msr_inventory_type';
  }

  public function sop_get($where='',$msrItemList='')
  {
    $this->db->select($this->trashtsop.'.*,'.$this->tbmi.'.description,m_currency.CURRENCY currency,t_msr_item.id_itemtype msr_id_itemtype,IFNULL('.$this->msrinv.'.description,\'\') as inv_desc, uom1.DESCRIPTION as uom1_desc, uom2.DESCRIPTION as uom2_desc');
    $this->db->join($this->tbmi, $this->tbmi.'.line_item = '.$this->trashtsop.'.msr_item_id', 'left');
    $this->db->join($this->tbeq_trash, 'trash_t_eq_data.msr_no=trash_t_sop.msr_no', 'left');
    $this->db->join($this->msrinv, 'trash_t_sop.inv_type=m_msr_inventory_type.id', 'left');
    $this->db->join('m_material_uom uom1', 'uom1.MATERIAL_UOM = trash_t_sop.uom1', 'left');
    $this->db->join('m_material_uom uom2', 'uom2.MATERIAL_UOM = trash_t_sop.uom2', 'left');
    $this->db->join('m_currency', 'm_currency.ID=trash_t_eq_data.currency', 'left');
    if(is_array($where))
    {
      $this->db->where($where);
    }
    if(is_array($msrItemList))
    {
      $this->db->where_in('trash_t_sop.msr_item_id',$msrItemList);
    }
    $sop = $this->db->get($this->trashtsop);
    return $sop;
  }

  public function getEd($msr_no='')
  {
    return $this->db->where(['msr_no'=>$msr_no])->get($this->tbeq_trash);
  }
  public function getTrashEdFromMsr($msr_no='')
  {
    return $this->db->select($this->tbeq_trash.'.*,'.$this->mdt.'.DELIVERYTERM_DESC,'.$this->mdp.'.DPOINT_DESC')
    ->join($this->mdt, $this->mdt.'.ID_DELIVERYTERM = '.$this->tbeq_trash.'.incoterm', 'left')
    ->join($this->mdp, $this->mdp.'.ID_DPOINT = '.$this->tbeq_trash.'.delivery_point', 'left')
    ->where([$this->tbeq_trash.'.msr_no' => $msr_no])
    ->get($this->tbeq_trash);
  }

  public function tbmi2Trash($msr_no='')
  {
    $s = $this->uri->rsegment(3);
    $join = '';
    $where = '';

    // original
    $this->db->where([$this->tbmi.'.msr_no'=>$msr_no]);
    /*$s = $this->uri->rsegment(3);
    if($s == 'nego')
    {
      $id = $this->session->ID;
      $this->db->select($this->tbmi.'.*, '.$this->tbdd.'.*, m_material_uom.DESCRIPTION as uom_desc');
      $this->db->join($this->tbdd, $this->tbdd.'.bled_no = '.$this->tbl_trash.'.bled_no and t_bid_detail.msr_detail_id = t_msr_item.line_item');
      $this->db->where([$this->tbdd.'.created_by'=>$id,'nego'=>1]);
    } else {
      $this->db->select($this->tbmi.'.*, m_material_uom.DESCRIPTION as uom_desc');
    }*/
    $this->db->select($this->tbmi.'.*, m_material_uom.DESCRIPTION as uom_desc');
    $this->db->join('m_material_uom', 'm_material_uom.MATERIAL_UOM = t_msr_item.uom');
    $s = $this->db->get($this->tbmi);
    
    return $s;
  }
}