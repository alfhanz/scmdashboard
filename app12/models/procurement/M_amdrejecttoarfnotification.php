<?php if (!defined('BASEPATH')) exit('Anda tidak masuk dengan benar');

class M_amdrejecttoarfnotification extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->arf  = 't_arf';

    $this->arfResponse  = 't_arf_response';
    $this->arfResponseTrash   = 'trash_t_arf_response';

    $this->arfResponseAttachment  = 't_arf_response_attachment';
    $this->arfResponseAttachmentTrash  = 'trash_t_arf_response_attachment';

    $this->arfResponseDetail  = 't_arf_response_detail';
    $this->arfResponseDetailTrash  = 'trash_t_arf_response_detail';

    $this->arfNotification  = 't_arf_notification';
    $this->arfNotificationTrash  = 'trash_t_arf_notification';

    $this->arfNotificationDetailRevision  = 't_arf_notification_detail_revision';
    $this->arfNotificationDetailRevisionTrash  = 'trash_t_arf_notification_detail_revision';

    $this->arfNotificationUpload  = 't_arf_notification_upload';
    $this->arfNotificationUploadTrash  = 'trash_t_arf_notification_upload';

    $this->arfSop  = 't_arf_sop';
    $this->arfSopTrash  = 'trash_t_arf_sop';

    $this->upload  = 't_upload';
    $this->uploadTrash  = 'trash_t_upload';

  }

  public function store($amd_no='')
  {
    $arf = $this->db->where('doc_no',$amd_no)->get($this->arf)->row();
    $arfResponse = $this->db->where('doc_no',$amd_no)->get($this->arfResponse)->row();
    $arfNotification = $this->db->where('doc_no',$amd_no)->get($this->arfNotification)->row();

    $this->db->trans_begin();
    
    $this->db->query("insert into ".$this->arfResponseTrash." select * from ".$this->arfResponse." where doc_no='$amd_no'");
    $this->db->where('doc_no', $amd_no)->delete($this->arfResponse);

    $this->db->query("insert into ".$this->arfResponseAttachmentTrash." select * from ".$this->arfResponseAttachment." where doc_id='".$arf->id."'");
    $this->db->where('doc_id', $arf->id)->delete($this->arfResponseAttachment);

    $this->db->query("insert into ".$this->arfResponseDetailTrash." select * from ".$this->arfResponseDetail." where doc_no='".$amd_no."'");
    $this->db->where('doc_no', $amd_no)->delete($this->arfResponseDetail);

    $this->db->query("insert into ".$this->arfNotificationTrash." select * from ".$this->arfNotification." where doc_no='".$amd_no."'");
    $this->db->where('doc_no', $amd_no)->delete($this->arfNotification);

    $this->db->query("insert into ".$this->arfNotificationTrash." select * from ".$this->arfNotification." where doc_no='".$amd_no."'");
    $this->db->where('doc_no', $amd_no)->delete($this->arfNotification);

    $this->db->query("insert into ".$this->arfNotificationDetailRevisionTrash." select * from ".$this->arfNotificationDetailRevision." where doc_id='".$arfNotification->id."'");
    $this->db->where('doc_id', $arfNotification->id)->delete($this->arfNotificationDetailRevision);

    $this->db->query("insert into ".$this->arfNotificationUploadTrash." select * from ".$this->arfNotificationUpload." where doc_id='".$arfNotification->id."'");
    $this->db->where('doc_id', $arfNotification->id)->delete($this->arfNotificationUpload);

    $this->db->query("insert into ".$this->arfSopTrash." select * from ".$this->arfSop." where doc_id='".$arfNotification->id."'");
    $this->db->where('doc_id', $arfNotification->id)->delete($this->arfSop);

    $this->db->query("insert into ".$this->arfSopTrash." select * from ".$this->arfSop." where doc_id='".$arfNotification->id."'");
    $this->db->where('doc_id', $arfNotification->id)->delete($this->arfSop);

    $this->db->query("insert into ".$this->uploadTrash." select * from ".$this->upload." where data_id='".$amd_no."' and module_kode = 'arf-recom-prep'");
    $this->db->where(['module_kode'=>'arf-recom-prep', 'data_id' => $amd_no])->delete($this->upload);

    if($this->db->trans_status() === true)
    {
      $this->db->trans_commit();
      return true;
    }
    else
    {
      $this->db->trans_rollback();
      return true;
    }
  }
}